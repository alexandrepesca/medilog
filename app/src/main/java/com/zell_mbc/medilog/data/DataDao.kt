package com.zell_mbc.medilog.data

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
abstract class DataDao {
    @Query("DELETE from data where _id = :id")
    abstract suspend fun delete(id: Int)

    @Query("DELETE from data where comment = :tmpComment")
    abstract suspend fun deleteTmpItem(tmpComment: String)

    @Query("DELETE FROM data")
    abstract suspend fun deleteAll()

    @Query("DELETE FROM data WHERE type=:dataType")
    abstract suspend fun deleteAll(dataType: Int)

    @Query("DELETE FROM data WHERE timestamp >=:start and timestamp <=:end")
    abstract suspend fun deleteAllFiltered(start: Long, end: Long)

    // ------------------------

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insert(obj: Data): Long

    @Update
    abstract suspend fun update(obj: Data)

    // ------
    @Query("SELECT MIN(CAST(value1 as float)) as value1 from data WHERE type=:dataType")
    abstract fun getMinValue1(dataType: Int): Float

    @Query("SELECT MIN(CAST(value1 as float)) as value1 from data WHERE type=:dataType and timestamp >=:start")
    abstract fun getMinValue1GreaterThan(dataType: Int, start: Long): Float

    @Query("SELECT MIN(CAST(value1 as float)) as value1 from data WHERE type=:dataType and timestamp <=:end")
    abstract fun getMinValue1LessThan(dataType: Int, end: Long): Float

    @Query("SELECT MIN(CAST(value1 as float)) as value1 from data WHERE type=:dataType and timestamp >=:start and timestamp <=:end")
    abstract fun getMinValue1Range(dataType: Int, start: Long, end: Long): Float
    // ------

    @Query("SELECT MAX(CAST(value1 as float)) as value1 from data WHERE type=:dataType")
    abstract fun getMaxValue1(dataType: Int): Float

    @Query("SELECT MAX(CAST(value1 as float)) as value1 from data WHERE type=:dataType and timestamp >=:start")
    abstract fun getMaxValue1GreaterThan(dataType: Int, start: Long): Float

    @Query("SELECT MAX(CAST(value1 as float)) as value1 from data WHERE type=:dataType and timestamp <=:end")
    abstract fun getMaxValue1LessThan(dataType: Int, end: Long): Float

    @Query("SELECT MAX(CAST(value1 as float)) as value1 from data WHERE type=:dataType and timestamp >=:start and timestamp <=:end")
    abstract fun getMaxValue1Range(dataType: Int, start: Long, end: Long): Float
    // ------

    // ------
    @Query("SELECT MIN(CAST(value2 as float)) as value2 from data WHERE type=:dataType")
    abstract fun getMinValue2(dataType: Int): Float

    @Query("SELECT MIN(CAST(value2 as float)) as value2 from data WHERE type=:dataType and timestamp >=:start")
    abstract fun getMinValue2GreaterThan(dataType: Int, start: Long): Float

    @Query("SELECT MIN(CAST(value2 as float)) as value2 from data WHERE type=:dataType and timestamp <=:end")
    abstract fun getMinValue2LessThan(dataType: Int, end: Long): Float

    @Query("SELECT MIN(CAST(value2 as float)) as value2 from data WHERE type=:dataType and timestamp >=:start and timestamp <=:end")
    abstract fun getMinValue2Range(dataType: Int, start: Long, end: Long): Float
    // ------

    @Query("SELECT MAX(CAST(value2 as float)) as value2 from data WHERE type=:dataType")
    abstract fun getMaxValue2(dataType: Int): Float

    @Query("SELECT MAX(CAST(value2 as float)) as value2 from data WHERE type=:dataType and timestamp >=:start")
    abstract fun getMaxValue2GreaterThan(dataType: Int, start: Long): Float

    @Query("SELECT MAX(CAST(value2 as float)) as value2 from data WHERE type=:dataType and timestamp <=:end")
    abstract fun getMaxValue2LessThan(dataType: Int, end: Long): Float

    @Query("SELECT MAX(CAST(value2 as float)) as value2 from data WHERE type=:dataType and timestamp >=:start and timestamp <=:end")
    abstract fun getMaxValue2Range(dataType: Int, start: Long, end: Long): Float
    // ------

    // ----------------------------
    @Query("SELECT COUNT(*) FROM data WHERE type=:dataType")
    abstract fun getSize(dataType: Int): Int

    @Query("SELECT COUNT(*) FROM data WHERE type=:dataType and timestamp >=:start")
    abstract fun getSizeGreaterThan(dataType: Int, start: Long): Int

    @Query("SELECT COUNT(*) FROM data WHERE type=:dataType and timestamp <=:end")
    abstract fun getSizeLessThan(dataType: Int, end: Long): Int

    @Query("SELECT COUNT(*) FROM data WHERE type=:dataType and timestamp >=:start and timestamp <=:end")
    abstract fun getSizeRange(dataType: Int, start: Long, end: Long): Int

    @Query("SELECT COUNT(*) FROM data")
    abstract fun getDBSize(): Int

    // ----------------------------

    @Query("SELECT * from data where _id = :id")
    abstract fun getItem(id: Int): Data

    // ----------------------------
    // getDataItems options, return type List
    // ------------ASC ------------
    @Query("SELECT * from data WHERE type=:dataType ORDER BY timestamp ASC") // Used by Chart fragment
    abstract fun getItemsASC(dataType: Int): List<Data>

    @Query("SELECT * from data WHERE type=:dataType and timestamp >=:start and timestamp <=:end ORDER BY timestamp ASC")
    abstract fun getItemsASCRange(dataType: Int, start: Long, end: Long): List<Data>

    @Query("SELECT * from data WHERE type=:dataType and timestamp >=:start ORDER BY timestamp ASC")
    abstract fun getItemsASCGreaterThan(dataType: Int, start: Long): List<Data>

    @Query("SELECT * from data WHERE type=:dataType and timestamp <=:end ORDER BY timestamp ASC")
    abstract fun getItemsASCLessThan(dataType: Int, end: Long): List<Data>

    // ------------DESC ------------
    @Query("SELECT * from data WHERE type=:dataType ORDER BY timestamp DESC")
    abstract fun getItemsDESC(dataType: Int): List<Data>

    @Query("SELECT * from data WHERE type=:dataType and timestamp >=:start and timestamp <=:end ORDER BY timestamp DESC")
    abstract fun getItemsDESCRange(dataType: Int, start: Long, end: Long): List<Data>

    @Query("SELECT * from data WHERE type=:dataType and timestamp >=:start ORDER BY timestamp DESC")
    abstract fun getItemsDESCGreaterThan(dataType: Int, start: Long): List<Data>

    @Query("SELECT * from data WHERE type=:dataType and timestamp <=:end ORDER BY timestamp DESC")
    abstract fun getItemsDESCLessThan(dataType: Int, end: Long): List<Data>

    // ----------------------------
    // getDataItems options, return type LiveData
    // Used by Recycler view
    @Query("SELECT * from data WHERE type=:dataType and timestamp >=:start and timestamp <=:end ORDER BY timestamp DESC")
    abstract fun getItemsRange(dataType: Int, start: Long, end: Long): LiveData<List<Data>>

    @Query("SELECT * from data WHERE type=:dataType and timestamp >=:start ORDER BY timestamp DESC")
    abstract fun getItemsGreaterThan(dataType: Int, start: Long): LiveData<List<Data>>

    @Query("SELECT * from data WHERE type=:dataType and timestamp <=:end ORDER BY timestamp DESC")
    abstract fun getItemsLessThan(dataType: Int, end: Long): LiveData<List<Data>>

    @Query("SELECT * from data WHERE type=:dataType ORDER BY timestamp DESC")
    abstract fun getItems(dataType: Int): LiveData<List<Data>>

    // ----------------------
    @Query("SELECT _id, timestamp, comment, type, SUM(CAST(value1 as int)) as value1, value2, value3, value4 from data WHERE type=:dataType group by value2")
    abstract fun getDays(dataType: Int): List<Data>

    @Query("SELECT _id, timestamp, comment, type, SUM(CAST(value1 as int)) as value1, value2, value3, value4 from data WHERE type=:dataType and timestamp >=:start group by value2")
    abstract fun getDaysGreaterThan(dataType: Int, start: Long): List<Data>

    @Query("SELECT _id, timestamp, comment, type, SUM(CAST(value1 as int)) as value1, value2, value3, value4 from data WHERE type=:dataType and timestamp <=:end group by value2")
    abstract fun getDaysLessThan(dataType: Int, end: Long): List<Data>

    @Query("SELECT _id, timestamp, comment, type, SUM(CAST(value1 as int)) as value1, value2, value3, value4 from data WHERE type=:dataType and timestamp >=:start and timestamp <=:end group by value2")
    abstract fun getDaysRange(dataType: Int, start: Long, end: Long): List<Data>

    @Query("SELECT SUM(CAST(value1 as int)) as value1 from data WHERE type=:dataType and value2 = :day")
    abstract fun getDay(dataType: Int, day: String): LiveData<Int>

    // ##########################
    @Query("SELECT * from data") // Used by Backup routine
    abstract fun backup(): List<Data>

    // ############################################################
    // Settings table
    // ############################################################
    @Query("SELECT _id from settings WHERE type=:type AND _key=:key")
    abstract fun getId(type: Int, key: String): Int

    @Query("SELECT value from settings WHERE type=:type AND _key=:key")
    abstract fun getSetting(type: Int, key: String): String

//    @Query("UPDATE settings SET value=:value where type=:type and _key=:key")
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun setSetting(entry: Settings)

    @Query("UPDATE settings SET value=:value where type=:type and _key=:key")
    abstract fun setSetting(type: Int, key: String, value: String)

    // Count all settings
    @Query("SELECT COUNT(*) FROM settings")
    abstract fun countSettings(): Int

    // Count all settings for a datatype
    @Query("SELECT COUNT(*) FROM settings WHERE type=:dataType")
    abstract fun countSettings(dataType: Int): Int

    // Count dataTypes = unique tabs
    @Query("SELECT COUNT (DISTINCT type) FROM settings")
    abstract fun countTypes(): Int

    // Return unique data types = unique tabs
    @Query("SELECT DISTINCT type FROM settings")
    abstract fun getTypes(): List<Int>

    @Query("SELECT * from settings") // Used by Backup routine
    abstract fun backupSettings(): List<Settings>

    // Return integer type value for tab_label
    @Query("SELECT type FROM settings where _key='TAB_LABEL' AND value=:tabLabel")
    abstract fun getType(tabLabel: String): Int
}
