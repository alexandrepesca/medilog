package com.zell_mbc.medilog.data

import android.annotation.SuppressLint
import android.app.Application
import android.content.SharedPreferences
import android.graphics.pdf.PdfDocument
import android.net.Uri
import android.os.Looper
import android.util.Log
import androidx.core.content.FileProvider
import androidx.documentfile.provider.DocumentFile
import androidx.lifecycle.*
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.DATE_PATTERN
import com.zell_mbc.medilog.MainActivity.Companion.DATE_TIME_PATTERN
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity.Companion.KEY_PREF_DELIMITER
import com.zell_mbc.medilog.utillity.Preferences
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import net.lingala.zip4j.ZipFile
import net.lingala.zip4j.io.outputstream.ZipOutputStream
import net.lingala.zip4j.model.ZipParameters
import net.lingala.zip4j.model.enums.EncryptionMethod
import java.io.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.logging.Handler


abstract class ViewModel(application: Application): AndroidViewModel(application) {
    val app = application

    abstract var tabIcon: Int
    abstract val filterStartPref: String
    abstract val filterEndPref: String
    abstract val itemName: String

    private var dataDao     = MediLogDB.getDatabase(app).dataDao()

    private var userOutputService: UserOutputService = UserOutputServiceImpl(app, null)
    private var filterChanged = MutableLiveData(0L)
    private val authority = app.applicationContext.packageName + ".provider"
    private val lineSeparator = System.getProperty("line.separator")
    private val fileName = "MediLog.csv"

    var filterStart = 0L
    var filterEnd = 0L
    var dataType = 0

    // PDF margin defaults
    val pdfHeaderTop = 30
    val pdfHeaderBottom = 50
    val pdfDataTop = 70
    val pdfLineSpacing = 15
    val pdfLeftBorder = 10
    val pdfTimeTab = 85 // Left offset for time
    val pdfDataTab = 150

    var pdfRightBorder = 0 // Set in fragment based on canvas size - pdfLeftBorder
    var pdfDataBottom = 0 // Set in fragment based on canvas size

    lateinit var dataRepository: DataRepository
    lateinit var items: LiveData<List<Data>>

    @JvmField
    val preferences: SharedPreferences = Preferences.getSharedPreferences(app)
    @JvmField
    val separator = preferences.getString(KEY_PREF_DELIMITER, ",")

    @SuppressLint("SimpleDateFormat")
    var csvPattern = SimpleDateFormat(DATE_TIME_PATTERN)
    abstract fun createPdfDocument(filtered: Boolean): PdfDocument?

    // Constructor
    open fun init(dt: Int) {
        dataType = dt
        getFilter()
        dataRepository = DataRepository(dataType, dataDao, filterStart, filterEnd)
        items = Transformations.switchMap(filterChanged) { dataRepository.getItems(filterStart, filterEnd) }

}

    fun insert(item: Data): Long {
        var rowId = -1L
        runBlocking {
            val j = viewModelScope.launch(Dispatchers.IO) { rowId = dataRepository.insert(item) }
            j.join()
        }
        return rowId
    }

    fun update(item: Data) = viewModelScope.launch(Dispatchers.IO) {
        dataRepository.update(item)
    }

    fun getItem(id: Int): Data? {
        var item: Data? = null
        runBlocking {
            val j = viewModelScope.launch(Dispatchers.IO) {
                item = dataRepository.getItem(id)
            }
            j.join()
        }
        return item
    }

    fun getItems(order: String, filtered: Boolean): List<Data> {
        if (filtered) getFilter()
        lateinit var items: List<Data>
        runBlocking {
            val j = viewModelScope.launch(Dispatchers.IO) {
                items = if (filtered) dataRepository.getItems(order, filterStart, filterEnd)
                else dataRepository.getItems(order, 0, 0)
            }
            j.join()
        }
        return items
    }

    fun getFirst(filtered: Boolean): Data? {
        if (filtered) getFilter()
        var item: Data? = null
        runBlocking {
            val j = viewModelScope.launch(Dispatchers.IO) {
                item = if (filtered) dataRepository.getFirst(filterStart, filterEnd)
                else dataRepository.getFirst(0L, 0L)
            }
            j.join()
        }
        return item
    }

    fun getLast(filtered: Boolean): Data? {
        if (filtered) getFilter()
        var item: Data? = null
        runBlocking {
            val j = viewModelScope.launch(Dispatchers.IO) {
                item = if (filtered) dataRepository.getLast(filterStart, filterEnd)
                else dataRepository.getLast(0L, 0L)
            }
            j.join()
        }
        return item
    }

    // Used by editItem Fragments to delete leftover temporary entries
    fun deleteTmpItem() = viewModelScope.launch(Dispatchers.IO) { dataRepository.deleteTmpItem(MainActivity.tmpComment) }

    fun isTmpItem(comment: String?): Boolean { return (comment.equals(MainActivity.tmpComment)) }

    fun toStringDate(l: Long): String { return DateFormat.getDateInstance().format(l) }

    @SuppressLint("SimpleDateFormat")
    fun toStringTime(l: Long): String { return SimpleDateFormat("HH:mm").format(l) }

    fun delete(id: Int) = viewModelScope.launch(Dispatchers.IO) { dataRepository.delete(id) }

    // Resets the filter an deletes all rows
    fun deleteAll(filtered: Boolean) = viewModelScope.launch(Dispatchers.IO) {
        if (filtered) {
            // If no end date specified set fake one to avoid a '...where timestamp <:0' query
            var tmpFilter = filterEnd
            if (tmpFilter == 0L) tmpFilter = Calendar.getInstance().timeInMillis + (86400000 * 10) // Add a 10 days worth of milliseconds
            dataRepository.deleteAllFiltered(filterStart, tmpFilter)
        } else dataRepository.deleteAll()
    }

    // Deletes all entries of a given type
    fun deleteAll(dataType: Int) = viewModelScope.launch(Dispatchers.IO) { dataRepository.deleteAll(dataType) }

    fun getDataTableSize(): Int {
        var i = 0
        runBlocking {
            val j = viewModelScope.launch(Dispatchers.IO) {
                i = dataRepository.getDBSize()
            }
            j.join()
        }
        return i
    }

    fun getSize(filtered: Boolean): Int {
        if (filtered) getFilter()
        var i = 0
        runBlocking {
            val j = viewModelScope.launch(Dispatchers.IO) {
                i = if (filtered) dataRepository.getSize(filterStart, filterEnd)
                else dataRepository.getSize(0L, 0L)
                //       Log.d("ViewModel", "getSize: $i")
            }
            j.join()
        }
        return i
    }

    // Get # of records for a specific data type
    fun getSize(type: Int): Int {
        var i = 0
        runBlocking {
            val j = viewModelScope.launch(Dispatchers.IO) {
                i = dataRepository.getSize(type)
                //       Log.d("ViewModel", "getSize: $i")
            }
            j.join()
        }
        return i
    }

    fun getMaxValue1(filtered: Boolean): Float{
        if (filtered) getFilter()
        var f = 0f
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            f = if (filtered) dataRepository.getMaxValue1(filterStart, filterEnd)
            else dataRepository.getMaxValue1(0L, 0L)
        }
            j.join() }
        return f
    }

    fun getMinValue1(filtered: Boolean): Float{
        if (filtered) getFilter()
        var f = 0f
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            f = if (filtered) dataRepository.getMinValue1(filterStart, filterEnd)
            else dataRepository.getMinValue1(0L, 0L)
        }
            j.join() }
        return f
    }

    fun getMaxValue2(filtered: Boolean): Float{
        if (filtered) getFilter()
        var f = 0f
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            f = if (filtered) dataRepository.getMaxValue2(filterStart, filterEnd)
            else dataRepository.getMaxValue2(0L, 0L)
        }
            j.join() }
        return f
    }

    fun getMinValue2(filtered: Boolean): Float{
        if (filtered) getFilter()
        var f = 0f
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            f = if (filtered) dataRepository.getMinValue2(filterStart, filterEnd)
            else dataRepository.getMinValue2(0L, 0L)
        }
            j.join() }
        return f
    }

    fun getFilter() {
        filterStart = preferences.getLong(filterStartPref, 0L)
        filterEnd = preferences.getLong(filterEndPref, 0L)
    }

    fun setFilter(start: Long, end: Long) {
        val editor = preferences.edit()
        editor.putLong(filterStartPref, start)
        editor.putLong(filterEndPref, end)
        editor.apply()

        filterStart = start
        filterEnd = end
        filterChanged.value = (filterStart + filterEnd) // Trigger data refresh
    }

    fun resetFilter() {
        val editor = preferences.edit()
        editor.putLong(filterStartPref, 0L)
        editor.putLong(filterEndPref, 0L)
        editor.apply()
        filterStart = 0L
        filterEnd = 0L
        filterChanged.value = 0L
        Log.d("DataViewModel : ", "New value: " + filterChanged.value)
    }

    fun setTodayFilter() {
        // Set "today" filter
        val cal: Calendar = Calendar.getInstance()
        cal.set(Calendar.HOUR_OF_DAY, 0)
        cal.set(Calendar.MINUTE, 0)
        setFilter(cal.timeInMillis, 0)
    }

        // Write temporary CSV file
    fun writeCsvFile(filtered: Boolean): Uri? {
            val filePath = File(app.cacheDir, "")
            val fn = fileName.replace(".csv", "") + "-" + SimpleDateFormat(DATE_PATTERN).format(Date().time) + ".csv"
            val newFile = File(filePath, fn)

            val uri = FileProvider.getUriForFile(app, authority, newFile)
            try {
            val out = app.contentResolver.openOutputStream(uri)
            if (out != null) {
                val data = dataToCSV(filtered)
                out.run {
                    write(data.toByteArray(Charsets.UTF_8))
                    flush()
                    close()
                }
            }
        } catch (e: IOException) {
            userOutputService.showAndHideMessageForLong(app.getString(R.string.eCreateFile) + " " + newFile)
            return null
        }
        return uri
    }

    private fun dataToCSV(filtered: Boolean): String {
        val items = getItems("ASC", filtered)
        if (items.isEmpty()) {
            userOutputService.showAndHideMessageForLong(app.getString(R.string.noDataToExport))
            return ""
        }

        val sb = StringBuilder()

        // Create header
        var s = "timestamp$separator comment$separator type$separator value1$separator value2$separator value3$separator value4$lineSeparator"
        sb.append(s)

        for (item in items) {
            s = csvPattern.format(item.timestamp) + separator + item.comment + separator + item.type + separator + item.value1 + separator + item.value2 + separator + item.value3 + separator + item.value4 + lineSeparator
            sb.append(s)
        }
        return sb.toString()
    }

    fun getPdfFile(document: PdfDocument?): Uri? {
        if (document == null) {
            return null
        }
        val filePath = File(app.cacheDir, "")
        val fn = fileName.replace(".csv", "") + "-" + SimpleDateFormat(DATE_PATTERN).format(Date().time) + ".pdf"
        val newFile = File(filePath, fn)
        val uri = FileProvider.getUriForFile(app, authority, newFile)
        try {
            val out = app.contentResolver.openOutputStream(uri)
            if (out != null) {
                document.run { writeTo(out) }
                out.run {
                    flush()
                    close()
                }
            }
        } catch (e: IOException) {
            userOutputService.showAndHideMessageForLong(app.getString(R.string.eCreateFile) + " " + newFile)
            return null
        }
        return uri
    }

    fun writeBackup(uri: Uri, zipPassword: String, autoBackup: Boolean = false) {
        val vm = this
        viewModelScope.launch(Dispatchers.IO) {
            Backup(app, uri, zipPassword, vm).exportZIPFile(autoBackup)
        }
    }

    fun readBackup(uri: Uri, zipPassword: String) {
        val vm = this
        viewModelScope.launch(Dispatchers.IO) {
            if (zipPassword == "csv") Backup(app, uri, "", vm).importCSVFile()
            else                      Backup(app, uri, zipPassword, vm).importZIPFile()
        }
    }

    @SuppressLint("SimpleDateFormat")
    // https://github.com/srikanth-lingala/zip4j
    fun getZIP(document: PdfDocument?, zipPassword: String): Uri? {
        if (document == null) {
            userOutputService.showAndHideMessageForLong(fileName.replace(".csv", "") + " " + app.getString(R.string.noDataToExport))
            return null
        }

        val uri: Uri?

        val outputStream: OutputStream?
        val zipOutputStream: ZipOutputStream?

        val zipParameters = ZipParameters()
        if (zipPassword.isNotEmpty()) {
            zipParameters.isEncryptFiles = true
            zipParameters.encryptionMethod = EncryptionMethod.AES
        }

        // Create empty Zip file
        val zipFileName = app.getString(R.string.appName) + "-" + SimpleDateFormat(DATE_PATTERN).format(Date().time) + ".zip"
        try {
            val filePath = File(app.externalCacheDir, "")
            val newFile = File(filePath, zipFileName)
            uri = FileProvider.getUriForFile(app, authority, newFile)
            outputStream = app.contentResolver.openOutputStream(uri)
            if (outputStream == null) {
                userOutputService.showAndHideMessageForLong(app.getString(R.string.eCreateFile) + " " + zipFileName)
                return null
            }

            zipOutputStream = if (zipPassword.isEmpty()) ZipOutputStream(BufferedOutputStream(outputStream))
            else ZipOutputStream(BufferedOutputStream(outputStream), zipPassword.toCharArray())

        } catch (e: Exception) {
            userOutputService.showAndHideMessageForLong(app.getString(R.string.eCreateFile) + " " + zipFileName)
            e.printStackTrace()
            return null
        }

        // File 1
        val fileNames = arrayOfNulls<String>(1)
        fileNames[0] = app.getString(R.string.appName) + "-" + SimpleDateFormat(DATE_PATTERN).format(Date().time) + ".pdf"

        val zipContentOut = ByteArrayOutputStream()
        document.run {
            writeTo(zipContentOut)
            close()
        }

        zipParameters.fileNameInZip = fileNames[0]

            try {
                zipOutputStream.run {
                    putNextEntry(zipParameters)
                    write(zipContentOut.toByteArray())
                    closeEntry()
                }
            } catch (e: Exception) {
                userOutputService.showAndHideMessageForLong(app.getString(R.string.eCreateFile) + " " + fileNames[0])
                e.printStackTrace()
                return null
            }

        zipOutputStream.close()

        return uri
    }
}