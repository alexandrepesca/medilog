package com.zell_mbc.medilog.data

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.afollestad.materialdialogs.MaterialDialog
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.diary.DiaryEditActivity
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utillity.Preferences
import com.zell_mbc.medilog.weight.WeightEditActivity
import java.text.DateFormat

abstract class TabFragment: Fragment() {
//    abstract var activityClass: Class<AppCompatActivity>
    lateinit var userOutputService: UserOutputService
    lateinit var viewModel: ViewModel

    var quickEntry = true
    lateinit var preferences: SharedPreferences

    abstract fun editItem(index: Int)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        preferences = Preferences.getSharedPreferences(requireContext())
        quickEntry = preferences.getBoolean(SettingsActivity.KEY_PREF_QUICKENTRY, true)
    }

    fun initializeService(view: View) {
        userOutputService = UserOutputServiceImpl(requireContext(), view)
    }

    fun itemClicked(item: Data) {
        val dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
        MaterialDialog(requireContext()).show {
            title(text = getString(R.string.ItemClicked) + " " + dateFormat.format(item.timestamp))
            message(R.string.whatDoYouWant)
            @Suppress("DEPRECATION")
            neutralButton(R.string.cancel)
            positiveButton(R.string.delete) { deleteItem(item._id) }
            negativeButton(R.string.edit) { editItem(item._id) }
        }
        MainActivity.resetReAuthenticationTimer(requireContext()) // User is interacting so let's reset the timeout
    }

    fun launchActivity(intent: Intent, index: Int) {
        val extras = Bundle()
        val item = viewModel.getItem(index)
        if (item != null) {
            extras.putInt("ID", item._id)
            extras.putInt("DATA_TYPE", viewModel.dataType)
            intent.putExtras(extras)
            startActivity(intent)
        } // Save id for lookup by EditFragment
    }

    private fun deleteItem(index: Int) {
        // Remove from array
        val item = viewModel.getItem(index)
        if (item != null) {
            viewModel.delete(item._id)
        }
        userOutputService.showMessageAndWaitForLong(viewModel.itemName + " " + getString(R.string.word_item) + " " + getString(R.string.word_deleted))
    }

    fun setColourStyle(v: View, light: Boolean = false) {
        val preferences = Preferences.getSharedPreferences(requireContext())
        val colourStyle = preferences.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue))

        if (light)
            when (colourStyle) {
                this.getString(R.string.green) -> v.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorLightGreen))
                this.getString(R.string.red) -> v.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorLightRed))
                this.getString(R.string.gray) -> v.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorLightGray))
                else -> v.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorLightBlue))
            }
        else
            when (colourStyle) {
                this.getString(R.string.green) -> v.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryGreen))
                this.getString(R.string.red) -> v.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryRed))
                this.getString(R.string.gray) -> v.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryGray))
                else -> v.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryBlue))
            }
    }
}
