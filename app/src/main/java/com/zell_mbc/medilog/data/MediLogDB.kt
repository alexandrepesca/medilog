package com.zell_mbc.medilog.data

import android.content.Context
import androidx.room.*
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.zell_mbc.medilog.MainActivity.Companion.BLOODPRESSURE
import com.zell_mbc.medilog.MainActivity.Companion.DIARY
import com.zell_mbc.medilog.MainActivity.Companion.WATER
import com.zell_mbc.medilog.MainActivity.Companion.WEIGHT
import com.zell_mbc.medilog.utillity.Preferences
import com.zell_mbc.medilog.utillity.SQLCipherUtils
import com.zell_mbc.medilog.utillity.SQLCipherUtils.getDatabaseState
import net.sqlcipher.database.SQLiteDatabase
import net.sqlcipher.database.SupportFactory
import java.io.File
import java.util.*


@Entity(tableName = "data", indices = [Index(value = ["timestamp"]), Index(value = ["type"])])
data class Data(
        @PrimaryKey(autoGenerate = true) val _id: Int,
        var timestamp: Long,
        var comment: String = "",
        var type: Int,
        var value1: String = "",
        var value2: String = "",
        var value3: String = "",
        var value4: String = ""
)

// A key/value table to store all settings for the individual measurements
// E.g. Weight unit: 1, "unit","kg"
//      Water  unit: 4, "unit","ml"
@Entity(tableName = "settings", indices = [Index(value = ["type"]), Index(value = ["_key"])])
data class Settings(
        @PrimaryKey(autoGenerate = true) val _id: Int,
        var type: Int,
        var _key: String,
        var value: String
)


@Database(entities = [Data::class, Settings::class], version = 6, exportSchema = false)
    abstract class MediLogDB : RoomDatabase() {
    abstract fun dataDao(): DataDao
    abstract fun settingsDao(): SettingsDao

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        private val MIGRATION_1_2: Migration = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                // Create the new table
                database.execSQL("CREATE TABLE IF NOT EXISTS `weight_new` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `timestamp` INTEGER NOT NULL, `comment` TEXT NOT NULL, `weight` REAL NOT NULL)")
                database.execSQL("INSERT INTO weight_new (_id, timestamp, comment, weight) SELECT _id, timestamp, comment, weight FROM weight;")
                database.execSQL("DROP TABLE weight;")// Change the table name to the correct one
                database.execSQL("ALTER TABLE weight_new RENAME TO weight;")

                database.execSQL("CREATE TABLE IF NOT EXISTS `diary_new` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `timestamp` INTEGER NOT NULL, `comment` TEXT NOT NULL, `diary` TEXT NOT NULL)")
                database.execSQL("INSERT INTO diary_new (_id, timestamp, comment, diary) SELECT _id, timestamp, '', diary FROM diary;")
                database.execSQL("DROP TABLE diary;")// Change the table name to the correct one
                database.execSQL("ALTER TABLE diary_new RENAME TO diary;")

                database.execSQL("CREATE TABLE IF NOT EXISTS `bp_new` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `timestamp` INTEGER NOT NULL, `comment` TEXT NOT NULL, `sys` INTEGER NOT NULL, `dia` INTEGER NOT NULL, `pulse` INTEGER NOT NULL)")
                database.execSQL("INSERT INTO bp_new (_id, timestamp, comment, sys, dia, pulse) SELECT _id, timestamp, comment, systolic, diastolic, pulse FROM bloodpressure")
                database.execSQL("DROP TABLE bloodpressure")// Change the table name to the correct one
                database.execSQL("ALTER TABLE bp_new RENAME TO bloodpressure;")
            }
        }

        private val MIGRATION_2_3: Migration = object : Migration(2, 3) {
            override fun migrate(database: SupportSQLiteDatabase) {
                // Add the index
                database.execSQL("CREATE INDEX index_weight_timestamp on weight(timestamp)")
                database.execSQL("CREATE INDEX index_bloodpressure_timestamp on bloodpressure(timestamp)")
                database.execSQL("CREATE INDEX index_diary_timestamp on diary(timestamp)")
            }
        }

        private val MIGRATION_3_4: Migration = object : Migration(3, 4) {
            override fun migrate(database: SupportSQLiteDatabase) {
                    database.execSQL("ALTER TABLE diary ADD COLUMN state INTEGER NOT NULL  DEFAULT '0'")
                }
        }

        private val MIGRATION_4_5: Migration = object : Migration(4, 5) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("CREATE TABLE IF NOT EXISTS `water` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `timestamp` INTEGER NOT NULL, `comment` TEXT NOT NULL, `water` INTEGER NOT NULL, `day` TEXT NOT NULL)")
                database.execSQL("CREATE INDEX index_water_timestamp on water(timestamp)")
                database.execSQL("ALTER TABLE bloodpressure ADD COLUMN state INTEGER NOT NULL  DEFAULT '0'")
            }
        }

        private val MIGRATION_5_6: Migration = object : Migration(5, 6) {
            override fun migrate(database: SupportSQLiteDatabase) {
                // Migrate
                database.execSQL("CREATE TABLE IF NOT EXISTS `data` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `timestamp` INTEGER NOT NULL, `comment` TEXT NOT NULL, `type` INTEGER NOT NULL, `value1` TEXT NOT NULL, `value2` TEXT NOT NULL, `value3` TEXT NOT NULL, `value4` TEXT NOT NULL)")
                database.execSQL("CREATE INDEX index_data_type on data(type)")
                database.execSQL("CREATE INDEX index_data_timestamp on data(timestamp)")

                // Move measurement data
                database.execSQL("INSERT INTO data (timestamp, comment, type, value1, value2, value3, value4) SELECT timestamp, comment, $WEIGHT, weight, '', '', '' FROM weight")
                database.execSQL("INSERT INTO data (timestamp, comment, type, value1, value2, value3, value4) SELECT timestamp, comment, $BLOODPRESSURE, sys, dia, pulse, state FROM bloodpressure")
                database.execSQL("INSERT INTO data (timestamp, comment, type, value1, value2, value3, value4) SELECT timestamp, comment, $DIARY, diary, state, '', '' FROM diary")
                database.execSQL("INSERT INTO data (timestamp, comment, type, value1, value2, value3, value4) SELECT timestamp, comment, $WATER, water, day, '', '' FROM water")

                database.execSQL("CREATE TABLE IF NOT EXISTS `settings` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `type` INTEGER NOT NULL, `_key` TEXT NOT NULL, `value` TEXT NOT NULL)")
                database.execSQL("CREATE INDEX index_settings_type on settings(type)")
                database.execSQL("CREATE INDEX index_settings__key on settings(_key)")

                // Do not drop the old tables until the next DB upgrade is due, to provide a safety net in case something goes wrong during migration
            }
        }

        private val MIGRATION_6_7: Migration = object : Migration(6, 7) {
            override fun migrate(database: SupportSQLiteDatabase) {
                // We have kept the old tables until next DB upgrade is due to provide a safety net in case something goes wrong during migration
                database.execSQL("DROP TABLE weight")
                database.execSQL("DROP TABLE bloodpressure")
                database.execSQL("DROP TABLE diary")
                database.execSQL("DROP TABLE water")
            }
        }

        @Volatile
        private var INSTANCE: MediLogDB? = null

        fun getDatabase(
                context: Context
        ) : MediLogDB {
            val encryptedSharedPreferences = Preferences.getEncryptedOrSharedPreferences(context) // Load encrypted preferences

            val dbName = "MediLogDatabase"
            val tempInstance = INSTANCE
            if (tempInstance != null) return tempInstance

            // Check if DB is already encrypted
            SQLiteDatabase.loadLibs(context)
            val dbFile = context.getDatabasePath(dbName)

//            dbFile.delete()
            val state = getDatabaseState(dbFile)
            var databasePin = encryptedSharedPreferences.getString("databasePin", "") // On first run or while migrating from unencrypted to encrypted this will return ""
            if (databasePin.isNullOrEmpty()) { // Pin not yet migrated to encrypted? Try unencrypted
                val sp = Preferences.getSharedPreferences(context)
                databasePin = sp.getString("databasePin", UUID.randomUUID().toString()) // Returns the old unencrypted pin or a new one
            }
            synchronized(this) {
                lateinit var instance: MediLogDB

                if (state == SQLCipherUtils.State.UNENCRYPTED) {
                    // Migrate to encrypted DB
                    val tmpDB = File.createTempFile("migration", "", context.cacheDir)
                    var database = SQLiteDatabase.openOrCreateDatabase(dbFile, "", null)
                    database.rawExecSQL(java.lang.String.format("ATTACH DATABASE '%s' AS encrypted KEY '%s';", tmpDB.absolutePath, databasePin))
                    database.rawExecSQL("SELECT sqlcipher_export('encrypted')")
                    database.rawExecSQL("DETACH DATABASE encrypted;")
                    val version = database.version
                    database.close()
                    database = SQLiteDatabase.openDatabase(tmpDB.absolutePath, databasePin, null, SQLiteDatabase.OPEN_READWRITE)
                    database.version = version
                    database.close()
                    dbFile.delete()
                    tmpDB.renameTo(dbFile)
                }

                val passphrase: ByteArray = SQLiteDatabase.getBytes(databasePin?.toCharArray())
                val factory = SupportFactory(passphrase)
                instance = Room.databaseBuilder(
                        context.applicationContext,
                        MediLogDB::class.java,
                        dbName)
                        .openHelperFactory(factory)
                        .addMigrations(MIGRATION_1_2)
                        .addMigrations(MIGRATION_2_3)
                        .addMigrations(MIGRATION_3_4)
                        .addMigrations(MIGRATION_4_5)
                        .addMigrations(MIGRATION_5_6)
                        .build()
                        //.fallbackToDestructiveMigration()

                INSTANCE = instance

                // If pin wasn't encrypted up to now, saving it here will fix that
                var editor = encryptedSharedPreferences.edit()
                editor?.putString("databasePin", databasePin)
                editor?.apply()

                // Set flags in regular/unencrypted SharedPreferences
                val sp = Preferences.getSharedPreferences(context)
                editor = sp.edit()
                editor?.putBoolean("encryptedDB", true)
                editor?.putBoolean("encryptedDBKey", true)
                editor?.apply()

                return instance
            }
        }

    }
}

