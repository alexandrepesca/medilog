package com.zell_mbc.medilog.data

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
abstract class SettingsDao {
    @Query("SELECT _id from settings WHERE type=:type AND _key=:key")
    abstract fun getId(type: Int, key: String): Int

    @Query("SELECT value from settings WHERE type=:type AND _key=:key")
    abstract fun getSetting(type: Int, key: String): String?

    //    @Query("UPDATE settings SET value=:value where type=:type and _key=:key")
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun set(entry: Settings)

    @Query("UPDATE settings SET value=:value where type=:type and _key=:key")
    abstract fun setSetting(type: Int, key: String, value: String)

    // Count all settings
    @Query("SELECT COUNT(*) FROM settings")
    abstract fun countSettings(): Int

    // Count all settings for a datatype
    @Query("SELECT COUNT(*) FROM settings WHERE type=:dataType")
    abstract fun countSettings(dataType: Int): Int

    // Count dataTypes = unique tabs
    @Query("SELECT COUNT (DISTINCT type) FROM settings")
    abstract fun countTypes(): Int

    // Return unique data types = unique tabs
    @Query("SELECT DISTINCT type FROM settings")
    abstract fun getDataTypes(): List<Int>

    @Query("SELECT * from settings") // Used by Backup routine
    abstract fun backup(): List<Settings>

    // Return integer type value for tab_label
    @Query("SELECT type FROM settings where _key='TAB_LABEL' AND value=:tabLabel")
    abstract fun getType(tabLabel: String): Int

    // Delete all settings for a data type
    @Query("DELETE FROM settings WHERE type=:dataType")
    abstract suspend fun deleteSettings(dataType: Int)

    // Empty Table
    @Query("DELETE FROM settings")
    abstract suspend fun deleteSettings()
}
