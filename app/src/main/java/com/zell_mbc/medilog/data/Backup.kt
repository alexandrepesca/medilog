package com.zell_mbc.medilog.data

import android.annotation.SuppressLint
import android.app.Application
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.LinearLayout
import androidx.documentfile.provider.DocumentFile
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utillity.Preferences
import net.lingala.zip4j.io.inputstream.ZipInputStream
import net.lingala.zip4j.io.outputstream.ZipOutputStream
import net.lingala.zip4j.model.LocalFileHeader
import net.lingala.zip4j.model.ZipParameters
import net.lingala.zip4j.model.enums.EncryptionMethod
import java.io.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class Backup(application: Application, private val uri: Uri, private val zipPassword: String, private val viewModel: ViewModel) {
    private val app = application
    private var userOutputService: UserOutputService = UserOutputServiceImpl(app, null)
    private val preferences: SharedPreferences = Preferences.getSharedPreferences(app)

    private val lineSeparator = System.getProperty("line.separator")
    private val separator = preferences.getString(SettingsActivity.KEY_PREF_DELIMITER, ",")

    @SuppressLint("SimpleDateFormat")
    var csvPattern = SimpleDateFormat(MainActivity.DATE_TIME_PATTERN)

    private var importDelimiter = ""

    // Create,write and protect backup ZIP file to backup location
    @SuppressLint("SimpleDateFormat")
    fun exportZIPFile(autoBackup: Boolean) {

        try {
            app.contentResolver.takePersistableUriPermission(uri, Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
            }
        catch (e: Exception) {
            Handler(Looper.getMainLooper()).post { userOutputService.showAndHideMessageForLong(app.getString(R.string.exception) + ": " + e.toString()) }
            return
        }

        // Create empty Zip file
        val zipFile = app.getString(R.string.appName) + "-Backup-" + SimpleDateFormat(MainActivity.DATE_PATTERN).format(Date().time) + ".zip"
        var dFile: DocumentFile? = null
        var dFolder: DocumentFile? = null
        try {
            dFolder = DocumentFile.fromTreeUri(app, uri)
        } catch (e: SecurityException) {
            Handler(Looper.getMainLooper()).post { userOutputService.showAndHideMessageForLong(app.getString(R.string.eCreateFile) + ": " + dFile + " " + e.toString()) }
            return
        }
        try {
            val canRead = dFolder?.canRead()
            var canWrite = dFolder?.canWrite()
            // Try to grab read access
            if (canWrite == false) {
                app.contentResolver.takePersistableUriPermission(uri, Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
            }
            canWrite = dFolder?.canWrite()
            if (canWrite == false) { // lost access rights, needs a manual backup
                Handler(Looper.getMainLooper()).post { userOutputService.showAndHideMessageForLong(app.getString(R.string.eLostAccessRights) + ": " + dFolder.toString()) }
                return
            }
            if (dFolder != null) dFile = dFolder.createFile("application/zip", zipFile)

            if (dFile == null) {
                Handler(Looper.getMainLooper()).post { userOutputService.showAndHideMessageForLong(app.getString(R.string.eUnableToWriteToFolder) + ": " + dFolder.toString()) }
                return
            }
        } catch (e: IllegalArgumentException) {
            Handler(Looper.getMainLooper()).post { userOutputService.showAndHideMessageForLong(app.getString(R.string.eCreateFile) + ": " + dFile + " " + e.toString()) }
            return
        }

        // Add files to ZIP file
        val dest: OutputStream?
        val out: ZipOutputStream?

        try {
            dest = app.contentResolver.openOutputStream(dFile.uri)
            if (dest == null) {
                Handler(Looper.getMainLooper()).post { userOutputService.showAndHideMessageForLong(app.getString(R.string.eCreateFile) + " " + dFile) }
                return
            }
            out = if (zipPassword.isEmpty()) ZipOutputStream(BufferedOutputStream(dest))
            else ZipOutputStream(BufferedOutputStream(dest), zipPassword.toCharArray())

        } catch (e: Exception) {
            Handler(Looper.getMainLooper()).post { userOutputService.showAndHideMessageForLong(app.getString(R.string.eCreateFile) + " " + dFile) }
            e.printStackTrace()
            return
        }
        val zipParameters = ZipParameters()
        if (zipPassword.isNotEmpty()) {
            zipParameters.isEncryptFiles = true
            zipParameters.encryptionMethod = EncryptionMethod.AES
        }

        val fileNames = arrayOfNulls<String>(1)
        fileNames[0] = app.getString(R.string.appName) + "-" + SimpleDateFormat(MainActivity.DATE_PATTERN).format(Date().time) + ".csv"

        val zipData = arrayOfNulls<String>(1)
        zipData[0] = dataTableToCSV()
        for (i in zipData.indices) {
            try {
                zipParameters.fileNameInZip = fileNames[i]
                out.putNextEntry(zipParameters)
                out.write(zipData[i]!!.toByteArray())
                out.closeEntry()
            } catch (e: Exception) {
                Handler(Looper.getMainLooper()).post { userOutputService.showAndHideMessageForLong(app.getString(R.string.eCreateFile) + " " + fileNames[i]) }
                e.printStackTrace()
                return
            }
        }
        try {
            out.close()
        } catch (e: Exception) {
            Handler(Looper.getMainLooper()).post { userOutputService.showAndHideMessageForLong(app.getString(R.string.eCreateFile) + " 2 " + dFile) }
            e.printStackTrace()
            return
        }
        var msg = if (zipPassword.isEmpty()) app.getString(R.string.unprotectedZipFileCreated)
        else app.getString(R.string.protectedZipFileCreated)

        if (autoBackup) msg = "AutoBackup: $msg"
        Handler(Looper.getMainLooper()).post { userOutputService.showAndHideMessageForLong(msg + " " + dFile.name) }

        // Capture last backup date
        val editor = preferences.edit()
        editor.putLong("LAST_BACKUP", Date().time)
        editor.putString(SettingsActivity.KEY_PREF_BACKUP_URI, uri.toString())
        editor.apply()
    }

    // Transfer whole data table into CSV string
    private fun dataTableToCSV(): String {
        val items = viewModel.dataRepository.dataBackup()

        if (items.isEmpty()) {
            userOutputService.showAndHideMessageForLong(app.getString(R.string.noDataToExport))
            return ""
        }

        val sb = StringBuilder()

        // Create header
        var s = "timestamp$separator comment$separator type$separator value1$separator value2$separator value3$separator value4$lineSeparator"
        sb.append(s)

        for (item in items) {
            s = csvPattern.format(item.timestamp) + separator + '"' +item.comment + '"' + separator + item.type + separator + '"' + item.value1 + '"' + separator + item.value2 + separator + item.value3 + separator + item.value4 + lineSeparator
            sb.append(s)
        }
        return sb.toString()
    }

    // Transfer settings table into CSV string
    fun settingsTableToCSV(): String {
/*        val items = viewModel.settingsRepository.settingsBackup()

        if (items.isEmpty()) {
            Handler(Looper.getMainLooper()).post { userOutputService.showAndHideMessageForLong(app.getString(R.string.noDataToExport)) }
            return ""
        }

        val sb = StringBuilder()

        // Create header
        var s = "type$separator key$separator value$separator$lineSeparator"
        sb.append(s)

        for (item in items) {
            s = csvPattern.format(item.type) + separator + '"' +item._key + '"' + separator + item.value + lineSeparator
            sb.append(s)
        } */
        return "" //sb.toString()
    }

    //#############################################

    fun importCSVFile() {
        val cr = app.contentResolver
        val inp: InputStream? = cr.openInputStream(uri)
        if (inp != null) {
            checkHeader(inp)
        }
    }

    fun importZIPFile(): Boolean {
        var localFileHeader: LocalFileHeader?
        var readLen: Int
        val readBuffer = ByteArray(4096)

        val zipInputStream = app.contentResolver.openInputStream(uri)

        // Try to open ZIP file
        val zipIn: ZipInputStream
        // With default password from settings
        try {
            zipIn = ZipInputStream(zipInputStream, zipPassword.toCharArray())
            localFileHeader = zipIn.nextEntry
        } catch (e: Exception) {
            Handler(Looper.getMainLooper()).post { userOutputService.showAndHideMessageForLong(app.getString(R.string.eOpenZipFile) + e.message) }
            return false
        }

        // Store file in temporary folder
        val filePath = File(app.externalCacheDir, "")

        while (localFileHeader != null) {
            val extractedFile = File(filePath, localFileHeader.fileName)
            // Check extension, if not csv no need to bother
            if (!extractedFile.name.endsWith(".csv")) {
                Handler(Looper.getMainLooper()).post { userOutputService.showAndHideMessageForLong(app.getString(R.string.eNoMediLogFile) + localFileHeader!!.fileName) }
                return false
            }

            val outputStream = FileOutputStream(extractedFile)

            readLen = zipIn.read(readBuffer)
            while (readLen != -1) {
                outputStream.write(readBuffer, 0, readLen)
                readLen = zipIn.read(readBuffer)
            }
            outputStream.close()

            val csvStream = FileInputStream(extractedFile)
            checkHeader(csvStream)
            localFileHeader = zipIn.nextEntry
        }
        return true
    }

    private fun checkHeader(inStream: InputStream) {
        val reader = BufferedReader(InputStreamReader(Objects.requireNonNull(inStream)))

        // Valid header?
        val line: String?
        try {
            line = reader.readLine()
        } catch (e: IOException) {
            Handler(Looper.getMainLooper()).post { userOutputService.showAndHideMessageForLong(app.getString(R.string.eReadError)) }
            return
        }

        // Empty file?
        if (line == null) {
            Handler(Looper.getMainLooper()).post { userOutputService.showAndHideMessageForLong(app.getString(R.string.eNoMediLogFile) + " line == null") }
            return
        }

        // Validate header line
        val headerLine = line.replace("\\s".toRegex(), "") // Remove potential blanks

        for (testDelimiter in app.getString(R.string.csvSeparators)) {
            if (headerLine.indexOf(testDelimiter) > 0) importDelimiter = testDelimiter.toString()
        }

        // Check if separator exists in headerLine
        if (importDelimiter.isEmpty()) {
            var filterHint = ""
            for (c in app.getString(R.string.csvSeparators)) filterHint = "$filterHint$c " // Add blanks for better visibility
            Handler(Looper.getMainLooper()).post { userOutputService.showAndHideMessageForLong(app.getString(R.string.noCsvDelimiterFound) + " $filterHint") }
            return
        }

        // Check header format
        val wantedHeader = "timestamp$importDelimiter" + "comment$importDelimiter" + "type$importDelimiter" + "value1$importDelimiter" + "value2$importDelimiter" + "value3$importDelimiter" + "value4"
        if (headerLine.toLowerCase(Locale.getDefault()).indexOf(wantedHeader) < 0) { // Header does not match
            Handler(Looper.getMainLooper()).post { userOutputService.showAndHideMessageForLong(app.getString(R.string.errorInCSVHeader) + ": '" + headerLine + "'") }
            return
        }

        readV2Data(reader)
    }

    private fun readV2Data(reader: BufferedReader?): Int {
        // Load data into buffer
        val tmpItems = ArrayList<Data>()
        if (reader == null) {
            return -1
        }
        try {
            var tmpLine: String
            var line: String?
            var lineNo = 0
            while (reader.readLine().also { line = it } != null) {
                lineNo++

                val item = Data(0, 0, "", 0, "", "", "", "") // Start with empty item
                tmpLine = line.toString() + importDelimiter // Add a delimiter to make sure the last value item is not overlooked

                val data = parseCSVLine(tmpLine) //.split(importDelimiter.toRegex()).toTypedArray()

                try {
                    val date: Date? = csvPattern.parse(data[0].trim())
                    if (date != null) item.timestamp = date.time
                } catch (e: ParseException) {
                    Handler(Looper.getMainLooper()).post {
                        userOutputService.showAndHideMessageForLong("Error converting date in line $lineNo: ${System.err} , '$tmpLine'")
                    }
                    return -2
                }

                item.comment = data[1].trim()

                try {
                    item.type = data[2].trim().toInt()
                } catch (e: NumberFormatException) {
                    Handler(Looper.getMainLooper()).post {
                        userOutputService.showAndHideMessageForLong("Error converting string to int in line $lineNo: ${System.err}, '$tmpLine'")
                    }
                    return -3
                }

                val size = data.size

                try {
                    if (size > 3) item.value1 = data[3].trim() // No value should never happen but just in case
                    if (size > 4) item.value2 = data[4].trim()
                    if (size > 5) item.value3 = data[5].trim()
                    if (size > 6) item.value4 = data[6].trim()
                    if (item.type == MainActivity.WATER) {
                        item.value2 = SimpleDateFormat("yyyyMMdd").format(item.timestamp)
                    }

                } catch (e: ParseException) {
                    Handler(Looper.getMainLooper()).post {
                        userOutputService.showAndHideMessageForLong("Error setting value in line $lineNo: ${System.err}, '$tmpLine'")
                    }
                    return -4
                }

                tmpItems.add(item)
            }
        } catch (e: IOException) {
            return -5
        }

        // If we arrived here, the import was successful
        val records = tmpItems.size
        if (records == 0) return -6     // Last check before deleting

        Handler(Looper.getMainLooper()).post {
            userOutputService.showAndHideMessageForLong("Read $records entries")
        }

        // Delete only after a successful import
        viewModel.deleteAll(false)

        for (item in tmpItems) {
            //MainActivity.viewModels[binding.tabLayout.selectedTabPosition].insert(item)
            viewModel.insert(item)
        }

        Handler(Looper.getMainLooper()).post { userOutputService.showAndHideMessageForLong("Imported $records records. ") }
        return records
    }


    // Parser which respects quotes -> To allow text entry with ,
    private fun parseCSVLine(csvLine: String): Array<String> {
        val separator = importDelimiter[0]
        val delimiter = '"'
        val lf = '\n'
        val cr = '\r'
        var quoteOpen = false
        val a = Array(10) { "" }
        var token = ""
        var column = 0
        for (c in csvLine.toCharArray()) when (c) {
            lf,
            cr -> {
                // not required as we are already read line
                quoteOpen = false
                a[column++] = token
                token = ""
            }
            delimiter -> quoteOpen = !quoteOpen
            separator -> {
                if (!quoteOpen) {
                    a[column++] = token
                    token = ""
                } else token += c
            }
            else -> token += c
        }
        return a
    }

}