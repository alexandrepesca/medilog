package com.zell_mbc.medilog.data

// Store all settings which are not hardcoded = New data types
class SettingsRepository(private val dao: SettingsDao) {

    fun get(type: Int, key: String): String? = dao.getSetting(type, key)
    fun getId(type: Int, key: String) = dao.getId(type, key)
    fun getDataTypes() = dao.getDataTypes()

    fun set(s: Settings) = dao.set(s)

    fun count(): Int = dao.countSettings()
    fun count(type: Int): Int = dao.countSettings(type)
    fun countTypes(): Int = dao.countTypes()
    fun getDataType(tabLabel: String) = dao.getType(tabLabel)

    suspend fun deleteSettings(type: Int) = dao.deleteSettings(type)
    suspend fun deleteSettings()          = dao.deleteSettings()

    fun settingsBackup() = dao.backup()
}