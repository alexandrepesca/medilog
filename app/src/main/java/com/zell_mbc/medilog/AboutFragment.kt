package com.zell_mbc.medilog

import android.content.pm.PackageManager.GET_SIGNATURES
import android.net.Uri
import android.os.Bundle
import android.text.util.Linkify
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.documentfile.provider.DocumentFile
import androidx.fragment.app.Fragment
import com.zell_mbc.medilog.databinding.ActivityAboutBinding
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utillity.Preferences
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.security.cert.CertificateException
import java.security.cert.CertificateFactory
import java.text.DateFormat
import java.util.*
import java.security.cert.X509Certificate as X509Certificate


class AboutFragment: Fragment() {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: ActivityAboutBinding? = null
    private val binding get() = _binding!!
    private lateinit var userOutputService : UserOutputService

    private fun initializeService(view: View) {
        userOutputService = UserOutputServiceImpl(requireContext(),view)
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = ActivityAboutBinding.inflate(inflater, container, false)
        initializeService(binding.root)
        return binding.root
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val preferences = Preferences.getSharedPreferences(requireContext())

        val versionCode = "Build " + BuildConfig.VERSION_CODE
        val versionName = "Version " + BuildConfig.VERSION_NAME

        binding.tvVersion.text = versionName
        binding.tvBuild.text = versionCode

        Linkify.addLinks(binding.tvEmail, Linkify.ALL)
        Linkify.addLinks(binding.tvMediLogLicenseUrl, Linkify.ALL)
        Linkify.addLinks(binding.tv3dPartyURL, Linkify.ALL)
        Linkify.addLinks(binding.tvMediLogUrl, Linkify.ALL)

        val sig = requireContext().packageManager.getPackageInfo(requireContext().packageName, GET_SIGNATURES).signatures[0]

//        val rawCert: Byte[] = sig.toByteArray()
        val certStream: InputStream = ByteArrayInputStream(sig.toByteArray())
        var subject = try {
            val certFactory = CertificateFactory.getInstance("X509")
            val x509Cert: X509Certificate = certFactory.generateCertificate(certStream) as X509Certificate
            x509Cert.subjectDN.toString()
        }
        catch (e: CertificateException) {
            "Unknown"
        }

        subject = "Certificate: $subject"
        binding.tvSigner.text = subject

        binding.tvEncryptedDB.text = if (preferences.getBoolean("encryptedDB", false))  getString(R.string.databaseEncryptionOn)
        else getString(R.string.databaseEncryptionOff)

        binding.tvAccessProtection.text = if (preferences.getBoolean(SettingsActivity.KEY_PREF_BIOMETRIC, false)) getString(R.string.biometricProtectionOn)
        else getString(R.string.biometricProtectionOff)

        val ss = preferences.getString(SettingsActivity.KEY_PREF_PASSWORD, "")
        if (ss != null) {
            if (ss.isNotEmpty()) binding.tvBackupProtection.text = getString(R.string.protectedBackupOn)
            else  binding.tvBackupProtection.text = getString(R.string.protectedBackupOff)
        }

        var timestamp = preferences.getLong("LAST_BACKUP", 0L)
        if (timestamp > 0) {
            val dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
            val timeFormat = DateFormat.getTimeInstance(DateFormat.SHORT)
            val lastBackup = dateFormat.format(timestamp) + " " + timeFormat.format(timestamp)
            binding.btLastBackup.text = lastBackup
        }
        else binding.btLastBackup.text = getString(R.string.unknown)

        binding.btLastBackup.setOnClickListener(View.OnClickListener {
            // Check if everything is in place for auto backups
            val uriString = preferences.getString(SettingsActivity.KEY_PREF_BACKUP_URI, "")
            if (uriString.isNullOrEmpty()) {
                userOutputService.showMessageAndWaitForLong("No Auto backup location set! Run one manual backup first")
                return@OnClickListener
            }

            // Valid location?
            val uri = Uri.parse(uriString)
            val dFolder = DocumentFile.fromTreeUri(requireContext(), uri)
            if (dFolder == null) {
                userOutputService.showMessageAndWaitForLong("Unable to use auto backup location set! Run a manual backup again.")
                return@OnClickListener
            }

            // Password
            var zipPassword = preferences.getString(SettingsActivity.KEY_PREF_PASSWORD, "")
            if (zipPassword.isNullOrEmpty()) zipPassword = ""

            MainActivity.viewModels[0].writeBackup(uri, zipPassword, false)

            timestamp = Date().time
            val editor = preferences.edit()
            editor.putLong("LAST_BACKUP_CHECK", timestamp)
            editor.apply()

            val dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
            val timeFormat = DateFormat.getTimeInstance(DateFormat.SHORT)
            val lastBackup = dateFormat.format(timestamp) + " " + timeFormat.format(timestamp)
            binding.btLastBackup.text = lastBackup
        })

        var s: String
        if (MainActivity.viewModels.size > 0) {
            val dbSize = MainActivity.viewModels[0].getDataTableSize()
            s = getString(R.string.database) + " " + getString(R.string.entries) + " "+ dbSize.toString()
            binding.tvDBEntries.text = s
        }

        val dbName = "MediLogDatabase"
        val f = context?.getDatabasePath(dbName)
        val dbSize = f?.length()
        if (dbSize != null) {
            s = getString(R.string.database) + " " + getString(R.string.size) + " "+ (dbSize/1024).toString() + " kb"
            binding.tvDbSize.text = s
        }
    }

}