package com.zell_mbc.medilog

import android.content.Context
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl

class BiometricHelper internal constructor(var context: Context) {
    private var biometricManager: BiometricManager = BiometricManager.from(context)
    private var userOutputService: UserOutputService = UserOutputServiceImpl(context,null)
    fun canAuthenticate(notify: Boolean): Int {
        val ret = -9999 // Unknown error
        when (biometricManager.canAuthenticate()) {
            BiometricManager.BIOMETRIC_SUCCESS -> {
 //               Log.d(context.getString(R.string.app_name), "App can authenticate using biometrics.")
                return 0
            }
            BIOMETRIC_ERROR_NO_HARDWARE -> {
 //               Log.e(context.getString(R.string.app_name), context.getString(R.string.noBiometricHardware))
                if (notify) {
                    run { userOutputService.showAndHideMessageForLong(context.getString(R.string.noBiometricHardware)) }
                }
                return -1
            }
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> {
//                Log.e(context.getString(R.string.app_name), context.getString(R.string.biometricHardwareUnavailable))
                run { userOutputService.showAndHideMessageForLong(context.getString(R.string.biometricHardwareUnavailable)) }
                return -2
            }
            BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
 //               Log.e(context.getString(R.string.app_name), context.getString(R.string.noCredentials))
                if (notify) {
                    run { userOutputService.showAndHideMessageForLong(context.getString(R.string.noCredentials)) }
                }
                return -3
            }
        }
        return ret
    }

    fun hasHardware(notify: Boolean): Boolean {
        when (biometricManager.canAuthenticate()) {
            BIOMETRIC_ERROR_NO_HARDWARE -> {
                if (notify) {
                    userOutputService.showAndHideMessageForLong(context.getString(R.string.noBiometricHardware))
                return false
                }
            }
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> {
                if (notify) {
                    userOutputService.showAndHideMessageForLong(context.getString(R.string.biometricHardwareUnavailable))
                }
                return false
            }
            BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
                return true
            }
            BiometricManager.BIOMETRIC_SUCCESS -> {
                return true
            }
        }
        return true
    }

}