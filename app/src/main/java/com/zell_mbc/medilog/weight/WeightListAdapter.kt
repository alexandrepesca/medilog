package com.zell_mbc.medilog.weight

import android.content.Context
import android.graphics.Color
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zell_mbc.medilog.MainActivity.Companion.TEXT_SIZE_DEFAULT
import com.zell_mbc.medilog.MainActivity.Companion.WEIGHT_THRESHOLD_DEFAULT
import com.zell_mbc.medilog.MainActivity.Companion.WEIGHT_UNIT_DEFAULT
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utillity.Preferences
import java.text.DateFormat

class WeightListAdapter internal constructor(context: Context) : RecyclerView.Adapter<WeightListAdapter.WeightViewHolder>() {
        private val inflater: LayoutInflater = LayoutInflater.from(context)
        private var clickListener: ItemClickListener? = null

        private var items = emptyList<Data>() // Cached copy of weights, no ide why I need it

        private var textSize = TEXT_SIZE_DEFAULT.toFloat()
        private var weightUnit: String?
        private var dateFormat: DateFormat
        private var timeFormat: DateFormat
        private val highlightValues: Boolean
        private var weightThreshold = WEIGHT_THRESHOLD_DEFAULT.toInt()

    inner class WeightViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val dateItem: TextView = itemView.findViewById(R.id.tvDateItem)
        val weightItem: TextView = itemView.findViewById(R.id.tvWeightItem)
        val commentItem: TextView = itemView.findViewById(R.id.tvCommentItem)

        override fun onClick(view: View) {
            if (clickListener != null) clickListener!!.onItemClick(view, adapterPosition)
        }

        init {
            dateItem.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            weightItem.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            commentItem.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            itemView.setOnClickListener(this)
        }
    }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeightViewHolder {
            val itemView = inflater.inflate(R.layout.weightview_row, parent, false)
            return WeightViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: WeightViewHolder, position: Int) {
            val orgColor = holder.dateItem.textColors.defaultColor

            val current = items[position]
            val tmpString = dateFormat.format(current.timestamp) + " - " + timeFormat.format(current.timestamp)
            holder.dateItem.text = tmpString

            val f = try {
                current.value1.toFloat()
            } catch  (e: NumberFormatException) {
                0f
            }

            val s = f.toString() + weightUnit // Type conversion to fix rounding errors
            if (highlightValues) {
                if (f >= weightThreshold) {
                    holder.weightItem.setTextColor(Color.rgb(255, 0, 0))
                    holder.weightItem.text = s
                } else {
                    holder.weightItem.setTextColor(orgColor)
                    holder.weightItem.text = s
                }
            }
            else holder.weightItem.text = s

            holder.commentItem.text = current.comment
        }

        internal fun setItems(items: List<Data>) {
            this.items = items
            notifyDataSetChanged()
        }

        fun getItemAt(position: Int): Data {
            return items[position]
        }

    // allows clicks events to be caught
    fun setClickListener(itemClickListener: ItemClickListener?) {
        clickListener = itemClickListener
    }


    // parent activity will implement this method to respond to click events
    interface ItemClickListener {
        fun onItemClick(view: View?, position: Int)
    }

    override fun getItemCount() = items.size

    init {
        val preferences = Preferences.getSharedPreferences(context)
        weightUnit = preferences.getString(SettingsActivity.KEY_PREF_WEIGHT_UNIT, WEIGHT_UNIT_DEFAULT)
        dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
        timeFormat = DateFormat.getTimeInstance(DateFormat.SHORT)

        var s = preferences.getString(SettingsActivity.KEY_PREF_TEXT_SIZE, TEXT_SIZE_DEFAULT)
        if (s != null) {
            textSize = try { s.toFloat() }
            catch  (e: NumberFormatException) { TEXT_SIZE_DEFAULT.toFloat() }
        }

        highlightValues = preferences.getBoolean(SettingsActivity.KEY_PREF_COLOUR, false)

        s = preferences.getString(SettingsActivity.KEY_PREF_WEIGHT_THRESHOLD, WEIGHT_THRESHOLD_DEFAULT)
        if (s != null) {
            weightThreshold = try { s.toInt() }
            catch  (e: NumberFormatException) { WEIGHT_THRESHOLD_DEFAULT.toInt() }
        }
    }
}
