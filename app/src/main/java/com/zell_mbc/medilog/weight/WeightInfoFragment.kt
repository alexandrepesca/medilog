package com.zell_mbc.medilog.weight

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.utillity.Preferences
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.WEIGHT
import com.zell_mbc.medilog.MainActivity.Companion.WEIGHT_UNIT_DEFAULT
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.databinding.WeightinfoformBinding
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity
import java.text.DateFormat
import kotlin.math.roundToLong

class WeightInfoFragment : Fragment() {
    private var _binding: WeightinfoformBinding? = null
    private val binding get() = _binding!!
    private lateinit var userOutputService : UserOutputService

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = WeightinfoformBinding.inflate(inflater, container, false)
        initializeService(binding.root)

        return binding.root
    }
    private fun initializeService(view: View) {
        userOutputService = UserOutputServiceImpl(requireContext(),view)
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private lateinit var viewModel: WeightViewModel  //by viewModels() //factoryProducer = { SavedStateViewModelFactory(this, ) })

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(requireActivity()).get(WeightViewModel::class.java)
        viewModel.init(WEIGHT)

        val preferences = Preferences.getSharedPreferences(requireContext())
        val weightUnit = " " + preferences.getString(SettingsActivity.KEY_PREF_WEIGHT_UNIT, WEIGHT_UNIT_DEFAULT)

        // Calculate BMI
        val count = viewModel.getSize(true)
        var item = viewModel.getLast(true)
        if (item == null) {
            userOutputService.showMessageAndWaitForLong(getString(R.string.noDataToShow))
            return
        }

        var sBMI = "N/A"
        val sBodyHeight = preferences.getString(SettingsActivity.KEY_PREF_BODY_HEIGHT, "")
        if (!sBodyHeight.isNullOrEmpty()){
            var fBodyHeight: Float
            try { fBodyHeight = sBodyHeight.toFloat()
                }
            catch (e: NumberFormatException) {
                fBodyHeight = 0f
                userOutputService.showMessageAndWaitForLong(getString(R.string.invalidBodyHeight))
            }

            val weight = try {
                item.value1.toFloat()
            } catch (e: NumberFormatException) {
                0f
            }

            if (weight > 0f && fBodyHeight > 0f) {
                var fBMI = weight / fBodyHeight / fBodyHeight * 100 * 100 // Height is entered in cm, formula expects m
                if (weightUnit.contains("lb")) fBMI *= 703f         // If imperial measures
                sBMI = ((fBMI * 100).roundToLong() / 100f).toString() // Round to 2 digits
            }
        }
        binding.tvBmiValue.text =  sBMI

        // Measurements
        var s = if ((viewModel.filterStart + viewModel.filterEnd) == 0L) getString(R.string.measurementsInDB) + " $count"
        else getString(R.string.measurementsInFilter) + " $count"
        binding.tvMeasurementCount.text = s

        val min = viewModel.getMinValue1(true)
        val max = viewModel.getMaxValue1(true)
        s = getString(R.string.minMaxValues) + " $min - $max $weightUnit"
        binding.tvMinMax.text = s

        val dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
        val endDate = dateFormat.format(item.timestamp)

        item = viewModel.getFirst(true)
        val startDate = dateFormat.format(item?.timestamp)

        s = getString(R.string.timePeriod) + " $startDate - $endDate"
        binding.tvTimePeriod.text = s

    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
        viewModel.deleteTmpItem() // Deletes all items with comment == tmpComment
    }
}
