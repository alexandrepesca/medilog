package com.zell_mbc.medilog.weight

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.zell_mbc.medilog.MainActivity

class WeightEditActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainActivity.setTheme(this)

        val intent: Intent = intent
        val itemID: Int = intent.getIntExtra("ID", 0)

        val f = WeightEditFragment().newInstance(itemID)
        supportFragmentManager
                .beginTransaction()
                .replace(android.R.id.content, f)
                .commit()
    }
}