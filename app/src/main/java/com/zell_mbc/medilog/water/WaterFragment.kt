package com.zell_mbc.medilog.water

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.WATER_THRESHOLD_DEFAULT
import com.zell_mbc.medilog.MainActivity.Companion.WATER_UNIT_DEFAULT
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.data.TabFragment
import com.zell_mbc.medilog.databinding.WaterTabBinding
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utillity.Preferences
import com.zell_mbc.medilog.weight.WeightEditActivity
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class WaterFragment : TabFragment(), WaterListAdapter.ItemClickListener {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: WaterTabBinding? = null
    private val binding get() = _binding!!

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = WaterTabBinding.inflate(inflater,container,false)
        initializeService(binding.root)

        return binding.root
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private var adapter: WaterListAdapter? = null
    private var waterThreshold = ""

    override fun editItem(index: Int) {
        val intent = Intent(requireContext(), WaterEditActivity::class.java)
        launchActivity(intent, index)
    }

    @SuppressLint("SimpleDateFormat")
    private fun addItem() {
        if (!quickEntry) {
            val tmpItem = Data(0, Date().time, MainActivity.tmpComment, viewModel.dataType, "", "", "", "")
            val itemID = viewModel.insert(tmpItem)
            if (itemID  != 0L) editItem(itemID.toInt())
            else userOutputService.showMessageAndWaitForLong("No entry with tmp value found!")
            return
        }

        // Check empty variables
        val waterValue = binding.etWater.text.toString()
        val commentValue = binding.etComment.text.toString()
        if (waterValue.isEmpty()) {
            userOutputService.showMessageAndWaitForLong(getString(R.string.waterMissing))
            return
        }
        var value = 0
        try {
            value = waterValue.toInt()
            if (value <= 0) {
                userOutputService.showMessageAndWaitForLong(this.getString(R.string.invalid) + " " + this.getString(R.string.water) + " " + this.getString(R.string.value) + " $value")
                return
            }
        } catch (e: Exception) {
            userOutputService.showMessageAndWaitForLong("Exception: " + this.getString(R.string.invalid) + " " + this.getString(R.string.water) + " " + this.getString(R.string.value) + " $value")
            return
        }
        val item = Data(0, Date().time, commentValue, viewModel.dataType, waterValue, SimpleDateFormat("yyyyMMdd").format(Date().time), "", "")

        viewModel.insert(item)

        if ((viewModel.filterEnd > 0L) && (viewModel.filterEnd < item.timestamp)) userOutputService.showMessageAndWaitForDuration(getString(R.string.filteredOut), 4000)

        binding.etWater.setText("")
        binding.etComment.setText("")

        // Close keyboard after entry is done
        (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken, 0)
    }

    private fun checkToday(waterToday: Int) {
        context ?: return

        val tmpString = requireContext().getString(R.string.today) + " " +waterToday.toString() + " / $waterThreshold" + binding.tvUnit.text  // + " (" + (waterThreshold.toInt()-waterToday).toString() + ")"
        binding.tvTodaysIntake.text = tmpString

        if (waterToday >= waterThreshold.toInt()) binding.tvTodaysIntake.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorWaterIntakeMet))
        else                                      binding.tvTodaysIntake.setTextColor(binding.tvUnit.textColors)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val tmpString = preferences.getString(SettingsActivity.KEY_PREF_WATER_THRESHOLD, WATER_THRESHOLD_DEFAULT)
        if (tmpString != null) waterThreshold = tmpString

        val addButton = binding.btAdd

        setColourStyle(binding.btAdd)
        addButton.setColorFilter(Color.WHITE)

        if (!quickEntry) { // Hide quick entry fields
            binding.etWater.visibility = View.GONE
            binding.etComment.visibility = View.GONE
            binding.tvUnit.visibility = View.GONE
        }
        else {
            binding.etWater.visibility = View.VISIBLE
            binding.etComment.visibility = View.VISIBLE
            binding.tvUnit.visibility = View.VISIBLE
            binding.tvUnit.text = preferences.getString(SettingsActivity.KEY_PREF_WATER_UNIT, WATER_UNIT_DEFAULT)
        }

        val layoutManager = LinearLayoutManager(requireContext())
        binding.rvWaterList.layoutManager = layoutManager

        adapter = WaterListAdapter(requireContext())
        viewModel = ViewModelProvider(requireActivity()).get(WaterViewModel::class.java) // This should return the MainActivity ViewModel

        viewModel.items.observe(requireActivity(), { water -> water?.let { adapter!!.setItems(it) } })

        val observer = Observer<Int> {
            if (it != null) checkToday(it)
            else checkToday(0)
        } // Looks like "it" is null when no value exists for today
        (viewModel as WaterViewModel).todaysIntake.observe(requireActivity(), observer) //Observer { checkToday(it) })

        adapter!!.setClickListener(this)
        binding.rvWaterList.adapter = adapter
        val dividerItemDecoration = DividerItemDecoration(binding.rvWaterList.context, layoutManager.orientation)
        binding.rvWaterList.addItemDecoration(dividerItemDecoration)

        // Respond to click events
        addButton.setOnClickListener { addItem() }

        setColourStyle(binding.btInfo, true)
        setColourStyle(binding.btChart, true)

        binding.btInfo.setOnClickListener(View.OnClickListener {
            context ?: return@OnClickListener
            val intent = Intent(requireContext(), WaterInfoActivity::class.java)
            startActivity(intent)
        })

        binding.btChart.setOnClickListener(View.OnClickListener {
            if (viewModel.getSize(true) < 2) {
                userOutputService.showMessageAndWaitForLong(requireContext().getString(R.string.notEnoughDataForChart))
                return@OnClickListener
            }

            val intent = Intent(requireContext(), WaterChartActivity::class.java)
            startActivity(intent)
        })

        binding.etWater.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if ((event.action == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                addItem()
                return@OnKeyListener true
            }
            false
        })

    }

    override fun onItemClick(view: View?, position: Int) {
        val item = adapter?.getItemAt(position)
        item ?: return
        itemClicked(item)
    }

}