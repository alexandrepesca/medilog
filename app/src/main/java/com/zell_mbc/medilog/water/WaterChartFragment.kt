package com.zell_mbc.medilog.water

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.DashPathEffect
import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.androidplot.util.PixelUtils
import com.androidplot.xy.*
import com.zell_mbc.medilog.MainActivity.Companion.WATER
import com.zell_mbc.medilog.MainActivity.Companion.WATER_THRESHOLD_DEFAULT
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utillity.*
import java.text.*
import java.util.*
import kotlin.math.max
import kotlin.math.roundToInt

// Chart manual
// https://github.com/halfhp/androidplot/blob/master/docs/xyplot.md
class WaterChartFragment : Fragment() {
    private var waterThreshold = ArrayList<Int>()
    private var waters = ArrayList<Int>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.water_chart, container, false)
    }

    @SuppressLint("SimpleDateFormat")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // create a couple arrays of y-values to plot:
        val labels = ArrayList<String>()
        var wMax = 0
        var wMin = 10000

        val preferences = Preferences.getSharedPreferences(requireContext())
        val barChart = preferences.getBoolean(SettingsActivity.KEY_PREF_WATER_BAR_CHART, true)
        var sTmp: String

        var fTmp: Int
        val simpleDate = SimpleDateFormat("MM-dd")

        val viewModel = ViewModelProvider(this).get(WaterViewModel::class.java)
        viewModel.init(WATER)
        val items = viewModel.getDays(true)
        for (wi in items) {
            sTmp = simpleDate.format(wi.timestamp)

            labels.add(sTmp)
            val waterInt = wi.value1.toInt()
            waters.add(waterInt)

            // Keep min and max values
            fTmp = waterInt
            if (fTmp > wMax) {
                wMax = fTmp
            }
            if (fTmp < wMin) {
                wMin = fTmp
            }
        }
        if (waters.size == 0) {
            return
        }

        // If threshold is set create dedicated chart, otherwise show as origin
        val thresholdValue = Integer.valueOf(preferences.getString(SettingsActivity.KEY_PREF_WATER_THRESHOLD, WATER_THRESHOLD_DEFAULT)!!)
        val threshold = preferences.getBoolean(SettingsActivity.KEY_PREF_SHOW_WATER_THRESHOLD, true)
        if (threshold) {
            for (item in waters) {
                waterThreshold.add(thresholdValue)
            }
        }

        // initialize our XYPlot reference:
        val plot: XYPlot = view.findViewById(R.id.waterPlot)
        PanZoom.attach(plot)

        val backgroundColor = getBackgroundColor(requireContext())
        plot.graph.backgroundPaint.color = backgroundColor

        val fontSizeSmall: Int = getFontSizeSmallInPx(requireContext())

        val axisPaint = Paint()
        axisPaint.style = Paint.Style.FILL_AND_STROKE
        axisPaint.color = getTextColorPrimary(requireContext())
        axisPaint.textSize = fontSizeSmall.toFloat()
        axisPaint.isAntiAlias = true

        val gridPaint = Paint()
        gridPaint.style = Paint.Style.STROKE
        gridPaint.color = getTextColorSecondary(requireContext())
        gridPaint.isAntiAlias = false
        gridPaint.pathEffect = DashPathEffect(floatArrayOf(3f, 2f), 0F)

        val showGrid = preferences.getBoolean(SettingsActivity.KEY_PREF_SHOW_WATER_GRID, true)
        if (!showGrid) {
            plot.graph.domainGridLinePaint = null
            plot.graph.rangeGridLinePaint = null
        }
        else {
            plot.graph.domainGridLinePaint = gridPaint
            plot.graph.rangeGridLinePaint = gridPaint // Horizontal lines
        }

        // Check text size and apply a factor to y-axis position
        // 42 for standard text size
        // 48 for large
        // 55 for largest
        val size = TextView(requireContext()).textSize
        var leftPadding = 0f
        if (size >= 48 ) leftPadding = 10f
        if (size >= 55 ) leftPadding = 25f
        plot.graph.paddingLeft = leftPadding

        val isLegendVisible = preferences.getBoolean(SettingsActivity.KEY_PREF_showWaterLegend, false)
        plot.legend.isVisible = isLegendVisible
        plot.legend.isDrawIconBackgroundEnabled = false

        val series1: XYSeries = SimpleXYSeries(waters, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, getString(R.string.water))
        val series2: XYSeries = SimpleXYSeries(waterThreshold, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, getString(R.string.threshold))

        val wMinBoundary = 0 // wMin - 10
        val wMaxBoundary = max(wMax, thresholdValue)  + 100
        plot.setRangeBoundaries(wMinBoundary, wMaxBoundary, BoundaryMode.FIXED)
        plot.setUserRangeOrigin(wMinBoundary / 10 * 10) // Set to a 10er value
        plot.setRangeStep(StepMode.INCREMENT_BY_VAL, 500.0)
        plot.outerLimits.set( 0, waters.size-1, wMinBoundary, wMaxBoundary) // For pan&zoom

        if (series1.size() < 10) plot.setDomainStep(StepMode.SUBDIVIDE, series1.size().toDouble()) // Avoid showing the same day multiple times
        plot.graph.getLineLabelStyle(XYGraphWidget.Edge.LEFT).format = DecimalFormat("####") // + waterUnit));  // Set integer y-Axis label

        // Line charts don't work without additional effort with date gaps, hence we switch to bar charts
        // Bar chart
        val waterBorder = ContextCompat.getColor(requireContext(), R.color.chart_sys_border)
        val waterFill = ContextCompat.getColor(requireContext(), R.color.chart_sys_fill)
        if (barChart) {
            val series1Format = BarFormatter(waterBorder, waterFill)
            plot.addSeries(series1, series1Format)
        } else {
            val series1Format = LineAndPointFormatter(waterBorder, null, waterFill, null)
            plot.addSeries(series1, series1Format)
        }

        if (threshold) {
            val formatThreshold = LineAndPointFormatter(Color.BLUE, null, null, null)
            formatThreshold.linePaint.pathEffect = DashPathEffect(floatArrayOf( // always use DP when specifying pixel sizes, to keep things consistent across devices:
                    PixelUtils.dpToPix(20f),
                    PixelUtils.dpToPix(15f)), 0f)

            formatThreshold.isLegendIconEnabled = false
            formatThreshold.linePaint.strokeWidth = 1f
            plot.addSeries(series2, formatThreshold)
        }
        plot.graph.getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).format = object : Format() {
            override fun format(obj: Any, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer {
                val i = (obj as Number).toFloat().roundToInt()
                return toAppendTo.append(labels[i])
            }

            override fun parseObject(source: String, pos: ParsePosition): Any {
                return 0
            }
        }
    }
}