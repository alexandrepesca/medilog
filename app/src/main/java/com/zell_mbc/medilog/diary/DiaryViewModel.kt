package com.zell_mbc.medilog.diary

import android.app.Application
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Typeface
import android.graphics.pdf.PdfDocument
import com.zell_mbc.medilog.*
import com.zell_mbc.medilog.MainActivity.Companion.DIARY
import com.zell_mbc.medilog.data.ViewModel
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity
import java.text.DateFormat
import java.util.*

class DiaryViewModel(application: Application): ViewModel(application) {
    override var tabIcon  = R.drawable.ic_diary
    override val filterStartPref = "DIARYFILTERSTART"
    override val filterEndPref = "DIARYFILTEREND"
    override var itemName = app.getString(R.string.diary)
    private var userOutputService: UserOutputService = UserOutputServiceImpl(app,null)

    override fun createPdfDocument(filtered: Boolean): PdfDocument? {
        if (getSize(filtered) == 0) {
            userOutputService.showAndHideMessageForLong( app.getString(R.string.diary) + " " + app.getString(R.string.noDataToExport))
            return null
        }
        val userName = preferences.getString(SettingsActivity.KEY_PREF_USER, "")
        val document = PdfDocument()


//        int tab1 = 80;
        val pdfDataTab = pdfTimeTab + 70
        var pageNumber = 1
        var i: Int
        var pageInfo: PdfDocument.PageInfo?
        var page: PdfDocument.Page
        var canvas: Canvas

        val formatedDate = DateFormat.getDateInstance(DateFormat.SHORT).format(Calendar.getInstance().time) + " - " + DateFormat.getTimeInstance(DateFormat.SHORT).format(Calendar.getInstance().time)

        val pdfPaint = Paint()
        pdfPaint.color = Color.BLACK
        val paintRed = Paint()
        paintRed.color = Color.RED
        val pdfPaintHighlight = Paint()
        pdfPaintHighlight.isFakeBoldText = true
        val pdfPaintItalics = Paint()
        pdfPaintItalics.typeface = Typeface.create(Typeface.DEFAULT, Typeface.ITALIC)
        val pdfPaintBoldItalics = Paint()
        pdfPaintBoldItalics.typeface = Typeface.create(Typeface.DEFAULT, Typeface.ITALIC)
        pdfPaintBoldItalics.isFakeBoldText = true

        // crate a A4 page description
        pageInfo = PdfDocument.PageInfo.Builder(595, 842, pageNumber).create()
        page = document.startPage(pageInfo)
        canvas = page.canvas

        pdfRightBorder = canvas.width - pdfLeftBorder
        pdfDataBottom = canvas.height - 15

        val pdfHeaderDateColumn = pdfRightBorder - 150

        // Draw header
        var headerText = app.getString(R.string.bpReportTitle)
        if (userName != null) headerText = headerText + " " + app.getString(R.string.forString) + " " + userName

        //                Log.d("--------------- Debug", "HeaderText:" + headerText);
        canvas.drawText(headerText, pdfLeftBorder.toFloat(), pdfHeaderTop.toFloat(), pdfPaint)
        canvas.drawText(app.getString(R.string.date) + ": " + formatedDate, pdfHeaderDateColumn.toFloat(), pdfHeaderTop.toFloat(), pdfPaint)
        canvas.drawLine(pdfLeftBorder.toFloat(), pdfHeaderBottom.toFloat(), pdfRightBorder.toFloat(), pdfHeaderBottom.toFloat(), pdfPaint)
        canvas.drawLine(pdfLeftBorder.toFloat(), pdfDataBottom.toFloat(), pdfRightBorder.toFloat(), pdfDataBottom.toFloat(), pdfPaint)

        i = pdfDataTop
        // Print header in bold
        canvas.drawText(app.getString(R.string.date), pdfLeftBorder.toFloat(), i.toFloat(), pdfPaintHighlight)
        canvas.drawText(app.getString(R.string.diary), pdfDataTab.toFloat(), i.toFloat(), pdfPaintHighlight)

        // ------------
        // Todo: Line breaks
        val items = getItems("ASC", filtered)
        for (item in items) {
            i += pdfLineSpacing
            // Start new page
            if (i > pdfDataBottom) {
                document.finishPage(page)
                pageNumber++

                // crate a A4 page description
                pageInfo = PdfDocument.PageInfo.Builder(595, 842, pageNumber).create()
                page = document.startPage(pageInfo)
                canvas = page.canvas
                canvas.drawText(headerText, pdfLeftBorder.toFloat(), pdfHeaderDateColumn.toFloat(), pdfPaint)
                canvas.drawText(app.getString(R.string.date) + ": " + formatedDate, pdfHeaderDateColumn.toFloat(), pdfHeaderTop.toFloat(), pdfPaint)
                canvas.drawLine(pdfLeftBorder.toFloat(), pdfHeaderBottom.toFloat(), pdfRightBorder.toFloat(), pdfHeaderBottom.toFloat(), pdfPaint)
                canvas.drawLine(pdfLeftBorder.toFloat(), pdfDataBottom.toFloat(), pdfRightBorder.toFloat(), pdfDataBottom.toFloat(), pdfPaint)
                i = pdfDataTop
                canvas.drawText(app.getString(R.string.date), pdfLeftBorder.toFloat(), i.toFloat(), pdfPaint)
                canvas.drawText(app.getString(R.string.diary), pdfDataTab.toFloat(), i.toFloat(), pdfPaint)
                // ------------
                i += pdfLineSpacing
            }
            var paint = pdfPaint

            // Check health state
            if (item.value2 == "1" ) paint = pdfPaintItalics
            if (item.value2 == "2" ) paint = pdfPaintBoldItalics
            canvas.drawText(toStringDate(item.timestamp), pdfLeftBorder.toFloat(), i.toFloat(), paint)
            canvas.drawText(item.value1, pdfDataTab.toFloat(), i.toFloat(), paint)
        }
        // finish the page
        document.finishPage(page)
        return document
    }

}
