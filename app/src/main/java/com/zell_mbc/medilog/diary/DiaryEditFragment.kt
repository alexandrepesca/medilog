package com.zell_mbc.medilog.diary

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.utillity.Preferences
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.DIARY
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.databinding.DiaryeditformBinding
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity
import java.text.DateFormat
import java.util.*

class DiaryEditFragment : DialogFragment() {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: DiaryeditformBinding? = null
    private val binding get() = _binding!!
    private lateinit var userOutputService : UserOutputService


    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = DiaryeditformBinding.inflate(inflater,container,false)
        initializeService(binding.root)

        return binding.root
    }
    private fun initializeService(view: View) {
        userOutputService = UserOutputServiceImpl(requireContext(),view)
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    lateinit var viewModel: DiaryViewModel //by viewModels() //factoryProducer = { SavedStateViewModelFactory(this, ) })
    var itemID: Int = 0
    private val timestampCal = Calendar.getInstance()

    private val GOOD = 0
    private val NOT_GOOD = 1
    private val BAD = 2

    private fun saveItem() {
        val editItem = viewModel.getItem(itemID)
        if (editItem == null) {
            userOutputService.showMessageAndWaitForLong("Unknown error!")
            return
        }

        // Check empty variables
        val value = binding.etDiary.text.toString()
        if (value.isEmpty()) {
            userOutputService.showMessageAndWaitForLong(getString(R.string.diaryMissing))
            return
        }

        editItem.value1 = value
        editItem.timestamp = timestampCal.timeInMillis
        editItem.comment = "" // Diary doesn't offer comments, hence we need to blank out the temp comment

        when (binding.rgState.checkedRadioButtonId) {
            binding.rbNotGood.id -> editItem.value2 = NOT_GOOD.toString()
            binding.rbBad.id     -> editItem.value2 = BAD.toString()
            else                 -> editItem.value2 = GOOD.toString()
        }

        viewModel.update(editItem)

        userOutputService.showMessageAndWaitForLong(requireContext().getString(R.string.itemUpdated))
        requireActivity().onBackPressed()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(requireActivity()).get(DiaryViewModel::class.java)
        viewModel.init(DIARY)

        val preferences = Preferences.getSharedPreferences(requireContext())
        val colourStyle = preferences.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue))
        val saveButton = binding.btSave

        saveButton.setColorFilter(Color.WHITE)

        when (colourStyle) {
            this.getString(R.string.green) -> binding.btSave.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryGreen))
            this.getString(R.string.red)   -> binding.btSave.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryRed))
            this.getString(R.string.gray)   -> binding.btSave.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryGray))
            else   -> binding.btSave.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryBlue))
        }

        val editItem = viewModel.getItem(itemID)
        if (editItem == null) {
            userOutputService.showMessageAndWaitForLong("Unknown error!")
            return
        }

        this.binding.etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(editItem.timestamp)
        this.binding.etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(editItem.timestamp)
        this.binding.etDiary.setText(editItem.value1)

        // Check if we are really editing or if this is a new value, if new entry delete tmpComment
        if (viewModel.isTmpItem(editItem.comment)) editItem.comment = ""

        val state = try { editItem.value2.toInt() }
        catch  (e: NumberFormatException) { GOOD }

        when (state) {
            NOT_GOOD -> this.binding.rbNotGood.isChecked = true
            BAD      -> this.binding.rbBad.isChecked = true
            else     -> this.binding.rbGood.isChecked = true
        }

        // Respond to click events
        val btSaveData: FloatingActionButton = view.findViewById(R.id.btSave)
        btSaveData.setOnClickListener { saveItem() }

        // ---------------------
        // Date/Time picker section
        timestampCal.timeInMillis = editItem.timestamp

        // create an OnDateSetListener
        val dateListener = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            timestampCal.set(Calendar.YEAR, year)
            timestampCal.set(Calendar.MONTH, monthOfYear)
            timestampCal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            binding.etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
            binding.etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
        }

        val timeListener = TimePickerDialog.OnTimeSetListener { _, hour, minute ->
            timestampCal.set(Calendar.HOUR_OF_DAY, hour)
            timestampCal.set(Calendar.MINUTE, minute)

            binding.etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
            binding.etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
            val b = android.text.format.DateFormat.is24HourFormat(requireContext())
            Log.d("--------------- Debug", "24hour: $b")
        }

//        btDatePicker!!.setOnClickListener(object : View.OnClickListener {
        binding.etDate.setOnClickListener {
            DatePickerDialog(requireContext(),
                    dateListener,
                    timestampCal.get(Calendar.YEAR),
                    timestampCal.get(Calendar.MONTH),
                    timestampCal.get(Calendar.DAY_OF_MONTH)).show()
        }

        binding.etTime.setOnClickListener {
            TimePickerDialog(requireContext(),
                    timeListener,
                    timestampCal.get(Calendar.HOUR_OF_DAY),
                    timestampCal.get(Calendar.MINUTE),
                    android.text.format.DateFormat.is24HourFormat(requireContext())).show()
        }

        // Make sure first field is highlighted and keyboard is open
        this.binding.etDiary.requestFocus()
    }

    override fun onPause() {
        super.onPause()
        MainActivity. resetReAuthenticationTimer(requireContext())
        viewModel.deleteTmpItem()
    }

    fun newInstance(i: Int): DiaryEditFragment {
        val f = DiaryEditFragment()
        // Supply index input as an argument.
        f.itemID = i
        //  Log.d("--------------- Debug", "NewInstance Index: " + itemID)
        return f
    }
}