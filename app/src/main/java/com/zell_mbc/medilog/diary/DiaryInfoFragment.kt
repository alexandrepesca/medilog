package com.zell_mbc.medilog.diary

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.DIARY
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.databinding.DiaryinfoformBinding
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import java.text.DateFormat

class DiaryInfoFragment : Fragment() {
    private var _binding: DiaryinfoformBinding? = null
    private val binding get() = _binding!!
    private lateinit var userOutputService : UserOutputService

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = DiaryinfoformBinding.inflate(inflater, container, false)
        initializeService(binding.root)

        return binding.root
    }
    private fun initializeService(view: View) {
        userOutputService = UserOutputServiceImpl(requireContext(),view)
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private lateinit var viewModel: DiaryViewModel  //by viewModels() //factoryProducer = { SavedStateViewModelFactory(this, ) })

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(requireActivity()).get(DiaryViewModel::class.java)
        viewModel.init(DIARY)

        // Calculate BMI
        val count = viewModel.getSize(true)
        var item = viewModel.getLast(true)
        if (item == null) {
            userOutputService.showMessageAndWaitForLong(getString(R.string.noDataToShow))
            return
        }

        // Measurements
        var s = if ((viewModel.filterStart + viewModel.filterEnd) == 0L) getString(R.string.measurementsInDB) + " $count"
        else getString(R.string.measurementsInFilter) + " $count"
        binding.tvMeasurementCount.text = s

        val dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
        val endDate = dateFormat.format(item.timestamp)

        item = viewModel.getFirst(true)
        val startDate = dateFormat.format(item?.timestamp)

        s = getString(R.string.timePeriod) + " $startDate - $endDate"
        binding.tvTimePeriod.text = s

    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
        viewModel.deleteTmpItem() // Deletes all items with comment == tmpComment
    }
}
