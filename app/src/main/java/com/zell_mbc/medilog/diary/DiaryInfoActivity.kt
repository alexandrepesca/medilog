package com.zell_mbc.medilog.diary

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.zell_mbc.medilog.MainActivity

class DiaryInfoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainActivity.setTheme(this)

        supportFragmentManager.beginTransaction()
                .replace(android.R.id.content, DiaryInfoFragment())
                .commit()
    }
}
