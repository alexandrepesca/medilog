package com.zell_mbc.medilog.custom

import android.app.Application
import android.graphics.pdf.PdfDocument
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.ViewModel
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import java.sql.Types

class CustomViewModel (application: Application): ViewModel(application) {
    override var tabIcon  = R.drawable.ic_noicon
    override var filterStartPref = ""
    override var filterEndPref = ""
    override var itemName = ""
    private var userOutputService: UserOutputService = UserOutputServiceImpl(app,null)

    // Constructor
    override fun init(dt: Int) {
        super.init(dt)
        filterStartPref = dataType.toString() + "_FILTERSTART"
        filterEndPref = dataType.toString() + "_FILTEREND"
    }


    override fun createPdfDocument(filtered: Boolean): PdfDocument? {
        val document = PdfDocument()
        return document
    }
}