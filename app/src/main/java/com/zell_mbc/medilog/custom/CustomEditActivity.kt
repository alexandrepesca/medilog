package com.zell_mbc.medilog.custom

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.zell_mbc.medilog.MainActivity

class CustomEditActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainActivity.setTheme(this)

        val intent: Intent = intent
        val itemID: Int = intent.getIntExtra("ID", 0)
        val dataType: Int = intent.getIntExtra("DATA_TYPE", 0)

        val f = CustomEditFragment().newInstance(itemID, dataType)
        supportFragmentManager
                .beginTransaction()
                .replace(android.R.id.content, f)
                .commit()
    }
}