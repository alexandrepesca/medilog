package com.zell_mbc.medilog.custom

import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.utillity.Preferences
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.FIRST_CUSTOM
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.SettingsViewModel
import com.zell_mbc.medilog.databinding.CustomtabseditformBinding
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity

class CustomTabsEditFragment : Fragment() {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: CustomtabseditformBinding? = null
    private val binding get() = _binding!!
    private lateinit var userOutputService : UserOutputService
    private lateinit var settings: SettingsViewModel  //by viewModels() //factoryProducer = { SavedStateViewModelFactory(this, ) })
    private var activeDataItem = 0
    private lateinit var customTabs: List<Int>

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = CustomtabseditformBinding.inflate(inflater,container,false)
        initializeService(binding.root)

        return binding.root
    }
    private fun initializeService(view: View) {
        userOutputService = UserOutputServiceImpl(requireContext(),view)
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    private fun saveItem() {

        val dataType = binding.tvTabID.text.toString().toInt()

        settings.setBoolean(dataType, "ACTIVE", binding.cbActive.isChecked)

        settings.setString(dataType, "TAB_LABEL", binding.etTabHeader.text.toString())
        settings.setString(dataType, "UNIT", binding.etUnit.text.toString())
        settings.setString(dataType, "LOWER_THRESHOLD", binding.etLowerThreshold.text.toString())
        settings.setString(dataType, "UPPER_THRESHOLD", binding.etUpperThreshold.text.toString())

        settings.setBoolean(dataType, "HIGHLIGHT_VALUES", binding.cbHighlightValue.isChecked)

        settings.setString(dataType, "VALUE_LENGTH",   binding.etFieldLength.text.toString())
        settings.setString(dataType, "VALUE_DATATYPE", binding.etDataType.text.toString())

/*        val editItem = viewModel.getItem(itemID)
        if (editItem == null) {
            userOutputService.showMessageAndWaitForLong("Unknown error!")
            return
        }

        // Check empty variables
        val value = binding.etCustom.text.toString()
        if (value.isEmpty()) {
            userOutputService.showMessageAndWaitForLong(this.getString(R.string.weightMissing))
            return
        }

        // Valid value?
        var customValue = 0
        try {
            customValue = value.toInt()
            if (customValue <= 0) {
                userOutputService.showMessageAndWaitForLong(this.getString(R.string.invalid) + " " + this.getString(R.string.weight) + " " + this.getString(R.string.value) + " $customValue")
                return
            }
        } catch (e: Exception) {
            userOutputService.showMessageAndWaitForLong("Exception: " + this.getString(R.string.invalid) + " " + this.getString(R.string.weight) + " " + this.getString(R.string.value) + " $customValue")
            return
        }

        editItem.timestamp = timestampCal.timeInMillis
        editItem.value1 = customValue.toString()
        editItem.comment = binding.etComment.text.toString()

        viewModel.update(editItem)
*/
        userOutputService.showMessageAndWaitForLong(requireContext().getString(R.string.itemUpdated))
//        requireActivity().onBackPressed()
    }

    private fun loadValues(dataType: Int) {
        binding.tvTabID.text = dataType.toString()
        var x: Boolean? = settings.getBoolean(dataType, "ACTIVE")
        if (x != null) binding.cbActive.isChecked = x

        val s = settings.getString(dataType, "TAB_LABEL")
        binding.etTabHeader.setText(s)

        binding.etUnit.setText(settings.getString(dataType, "UNIT"))
        binding.etLowerThreshold.setText(settings.getString(dataType, "LOWER_THRESHOLD"))
        binding.etUpperThreshold.setText(settings.getString(dataType, "UPPER_THRESHOLD"))

        x = settings.getBoolean(dataType, "HIGHLIGHT_VALUES")
        if (x != null) binding.cbHighlightValue.isChecked = x

        binding.etFieldLength.setText(settings.getString(dataType, "VALUE_LENGTH"))
        binding.etDataType.setText(settings.getString(dataType, "VALUE_DATATYPE"))
    }

    private fun previousDataItem() {
        if (activeDataItem > 0) activeDataItem -= 1
        loadValues(customTabs[activeDataItem])
    }

    private fun nextDataItem() {
        if (activeDataItem < customTabs.size-1) activeDataItem += 1
        loadValues(customTabs[activeDataItem])
    }

    private fun newDataItem() {
        var newType = FIRST_CUSTOM
        while (customTabs.contains(newType)) newType++
        loadValues(newType)
    }

    private fun loadDataTypeArray() {
        customTabs = settings.getDataTypes()
        if (customTabs.isEmpty()) newDataItem()
        else loadValues(customTabs[0])
        activeDataItem = 0
    }

    private fun deleteDataItem() {
        settings.deleteSettings(customTabs[activeDataItem])
        loadDataTypeArray()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        settings = ViewModelProvider(this).get(SettingsViewModel::class.java)
        settings.init()

        val preferences = Preferences.getSharedPreferences(requireContext())
        val colourStyle = preferences.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue))
        val saveButton = binding.btSave

        when (colourStyle) {
            this.getString(R.string.green) -> binding.btSave.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryGreen))
            this.getString(R.string.red)   -> binding.btSave.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryRed))
            this.getString(R.string.gray)   -> binding.btSave.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryGray))
            else   -> binding.btSave.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryBlue))
        }
        saveButton.setColorFilter(Color.WHITE)

        // Load dataType Array
        loadDataTypeArray()

//        binding.tvUnit.text = settings.getString(6, "UNIT")
//        binding.tvLabel.text = settings.getString(6, "TAB_LABEL")


        // Respond to click events
        saveButton.setOnClickListener { saveItem() }
        binding.btPrevious.setOnClickListener { previousDataItem() }
        binding.btNext.setOnClickListener { nextDataItem() }
        binding.btNew.setOnClickListener { newDataItem() }
        binding.btDelete.setOnClickListener { deleteDataItem() }

        // Make sure first field is highlighted and keyboard is open
        binding.etTabHeader.requestFocus()
    }

    override fun onPause() {
        super.onPause()
        MainActivity. resetReAuthenticationTimer(requireContext())
    }

}
