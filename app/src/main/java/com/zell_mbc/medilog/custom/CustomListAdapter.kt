package com.zell_mbc.medilog.custom

import android.content.Context
import android.graphics.Color
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zell_mbc.medilog.MainActivity.Companion.TEXT_SIZE_DEFAULT
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.Data
import java.sql.Types.FLOAT
import java.sql.Types.INTEGER
import java.text.DateFormat

class CustomListAdapter internal constructor(context: Context,
                                             private val customUnit: String,
                                             val highlightValues: Boolean,
                                             private val lowerThreshold: Int,
                                             private val upperThreshold: Int,
                                             private val valueDataType: Int,
) : RecyclerView.Adapter<CustomListAdapter.CustomViewHolder>() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var clickListener: ItemClickListener? = null

    private var items = emptyList<Data>()

    private var textSize = TEXT_SIZE_DEFAULT.toFloat()
    private var dateFormat: DateFormat
    private var timeFormat: DateFormat

    inner class CustomViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val dateItem: TextView = itemView.findViewById(R.id.tvDateItem)
        val customItem: TextView = itemView.findViewById(R.id.tvCustomItem)
        val commentItem: TextView = itemView.findViewById(R.id.tvCommentItem)

        override fun onClick(view: View) {
            if (clickListener != null) clickListener!!.onItemClick(view, adapterPosition)
        }

        init {
            dateItem.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            customItem.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            commentItem.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            itemView.setOnClickListener(this)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val itemView = inflater.inflate(R.layout.customview_row, parent, false)
        return CustomViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val orgColor = holder.dateItem.textColors.defaultColor

        val current = items[position]
        val tmpString = dateFormat.format(current.timestamp) + " - " + timeFormat.format(current.timestamp)
        holder.dateItem.text = tmpString

        val s = current.value1 + customUnit

        if (valueDataType == FLOAT) {
        }

        if (highlightValues) {
            when (valueDataType) {
                INTEGER -> {
                    val i = try { current.value1.toInt() }
                    catch (e: NumberFormatException) { 0 }

                    if ((lowerThreshold > 0 && i < lowerThreshold) || (upperThreshold in 1..i)) {
                        holder.customItem.setTextColor(Color.rgb(255, 0, 0))
                        holder.customItem.text = s
                    } else {
                        holder.customItem.setTextColor(orgColor)
                        holder.customItem.text = s
                    }
                }
                FLOAT -> {
                    val f = try {
                        current.value1.toFloat()
                    } catch (e: NumberFormatException) {
                        0f
                    }

                    if (f >= upperThreshold || f <= lowerThreshold) {
                        holder.customItem.setTextColor(Color.rgb(255, 0, 0))
                        holder.customItem.text = s
                    } else {
                        holder.customItem.setTextColor(orgColor)
                        holder.customItem.text = s
                    }
                }
                else -> {
                    holder.customItem.setTextColor(orgColor)
                    holder.customItem.text = s
                }
            }
        }
        else holder.customItem.text = s

        holder.commentItem.text = current.comment
    }

    internal fun setItems(items: List<Data>) {
        this.items = items
        notifyDataSetChanged()
    }

    fun getItemAt(position: Int): Data {
        return items[position]
    }

    // allows clicks events to be caught
    fun setClickListener(itemClickListener: ItemClickListener?) {
        clickListener = itemClickListener
    }


    // parent activity will implement this method to respond to click events
    interface ItemClickListener {
        fun onItemClick(view: View?, position: Int)
    }

    override fun getItemCount() = items.size

    init {
        dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
        timeFormat = DateFormat.getTimeInstance(DateFormat.SHORT)
    }
}
