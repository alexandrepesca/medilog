package com.zell_mbc.medilog.bloodpressure

import android.content.Context
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utillity.Preferences


class BloodPressureHelper(val context: Context) {
    var hyperGrade3Sys = 0
    var hyperGrade3Dia = 0
    var hyperGrade2Sys = 0
    var hyperGrade2Dia = 0
    var hyperGrade1Sys = 0
    var hyperGrade1Dia = 0

    val hyperGrade1 = 1
    val hyperGrade2 = 2
    val hyperGrade3 = 3
    val userOutputService = UserOutputServiceImpl(context, null)

    init {
        // Check values
        val preferences = Preferences.getSharedPreferences(context)
        var sTmp = preferences.getString(SettingsActivity.KEY_PREF_hyperGrade1, context.getString(R.string.grade1Values))
        try {
            val grade = sTmp!!.split(",".toRegex()).toTypedArray()
            hyperGrade1Sys = grade[0].toInt()
            hyperGrade1Dia = grade[1].toInt()
        } catch (e: Exception) {
            userOutputService.showAndHideMessageForLong(context.getString(R.string.grade1Error) + " " + sTmp + " , " + context.getString(R.string.gradeErrorEnd))
            e.printStackTrace()
        }
        sTmp = preferences.getString(SettingsActivity.KEY_PREF_hyperGrade2, context.getString(R.string.grade2Values))
        try {
            val grade = sTmp!!.split(",".toRegex()).toTypedArray()
            hyperGrade2Sys = grade[0].toInt()
            hyperGrade2Dia = grade[1].toInt()
        } catch (e: Exception) {
            userOutputService.showAndHideMessageForLong(context.getString(R.string.grade2Error) + " " + sTmp + " , " + context.getString(R.string.gradeErrorEnd))
            e.printStackTrace()
        }
        sTmp = preferences.getString(SettingsActivity.KEY_PREF_hyperGrade3, context.getString(R.string.grade3Values))
        try {
            val grade = sTmp!!.split(",".toRegex()).toTypedArray()
            hyperGrade3Sys = grade[0].toInt()
            hyperGrade3Dia = grade[1].toInt()
        } catch (e: Exception) {
            userOutputService.showAndHideMessageForLong(context.getString(R.string.grade3Error) + " " + sTmp + " , " + context.getString(R.string.gradeErrorEnd))
            e.printStackTrace()
        }
    }

    fun sysGrade(systolic: String): Int {
        val sys = try {
            systolic.toInt()
        }
        catch  (e: NumberFormatException) {
            0
        }

        if (sys >= hyperGrade3Sys) return hyperGrade3
        if (sys >= hyperGrade2Sys) return hyperGrade2
        if (sys >= hyperGrade1Sys) return hyperGrade1
        return 0
    }

    fun diaGrade(diastolic: String): Int {
        val dia = try {
            diastolic.toInt()
        }
        catch  (e: NumberFormatException) {
            0
        }

        if (dia >= hyperGrade3Dia) return hyperGrade3
        if (dia >= hyperGrade2Dia) return hyperGrade2
        if (dia >= hyperGrade1Dia) return hyperGrade1
        return 0
    }
}