package com.zell_mbc.medilog.bloodpressure

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.zell_mbc.medilog.MainActivity

class BloodPressureChartActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainActivity.setTheme(this)

        supportFragmentManager.beginTransaction()
                .replace(android.R.id.content, BloodPressureChartFragment())
                .commit()
    }
}