package com.zell_mbc.medilog.bloodpressure

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.zell_mbc.medilog.MainActivity

class BloodPressureEditActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainActivity.setTheme(this)

        val intent: Intent = intent
        val id: Int = intent.getIntExtra("ID", 0)

        val f = BloodPressureEditFragment().newInstance(id)
        supportFragmentManager
                .beginTransaction()
                .replace(android.R.id.content, f)
                .commit()
    }
}