package com.zell_mbc.medilog.bloodpressure

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.BLOODPRESSURE
import com.zell_mbc.medilog.MainActivity.Companion.BLOODPRESSURE_UNIT_DEFAULT
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.databinding.BloodpressureinfoformBinding
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utillity.Preferences
import java.text.DateFormat

class BloodPressureInfoFragment : Fragment() {
    private var _binding: BloodpressureinfoformBinding? = null
    private val binding get() = _binding!!
    private lateinit var userOutputService : UserOutputService

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = BloodpressureinfoformBinding.inflate(inflater, container, false)

        return binding.root
    }

    private fun initializeService(view: View) {
        userOutputService = UserOutputServiceImpl(requireContext(),view)
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private lateinit var viewModel: BloodPressureViewModel  //by viewModels() //factoryProducer = { SavedStateViewModelFactory(this, ) })

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initializeService(view)

        viewModel = ViewModelProvider(requireActivity()).get(BloodPressureViewModel::class.java)
        viewModel.init(BLOODPRESSURE)

        val preferences = Preferences.getSharedPreferences(requireContext())
        val bpUnit = " " + preferences.getString(SettingsActivity.KEY_PREF_BLOODPRESSURE_UNIT, BLOODPRESSURE_UNIT_DEFAULT)

        val count = viewModel.getSize(true)
        var item = viewModel.getLast(true)
        if (item == null) {
            userOutputService.showMessageAndWaitForLong(getString(R.string.noDataToShow))
            return
        }

        binding.tvBloodPressureHeader.text = "" //BloodPressure Header"         // Round to 2 digits

        // Measurements
        var s = if ((viewModel.filterStart + viewModel.filterEnd) == 0L) getString(R.string.entriesInDB) + " $count"
        else getString(R.string.entriesInFilter) + " $count"
        binding.tvMeasurementCount.text = s

        var min = viewModel.getMinValue1(true)
        var max = viewModel.getMaxValue1(true)
        binding.tvMinMax.text = getString(R.string.minMaxValues)
        s = getString(R.string.systolic) + " $min - $max $bpUnit"
        binding.tvSystolic.text = s

        min = viewModel.getMinValue2(true)
        max = viewModel.getMaxValue2(true)
        s = getString(R.string.diastolic) + " $min - $max $bpUnit"
        binding.tvDiastolic.text = s

        val dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
        val endDate = dateFormat.format(item.timestamp)

        item = viewModel.getFirst(true)
        val startDate = dateFormat.format(item?.timestamp)

        s = getString(R.string.timePeriod) + " $startDate - $endDate"
        binding.tvTimePeriod.text = s
    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
        viewModel.deleteTmpItem() // Deletes all items with comment == tmpComment
    }
}
