package com.zell_mbc.medilog.bloodpressure

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.BLOODPRESSURE
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.databinding.BloodpressureeditformBinding
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utillity.Preferences
import java.text.DateFormat
import java.util.*

class BloodPressureEditFragment : Fragment() {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: BloodpressureeditformBinding? = null
    private val binding get() = _binding!!
    private lateinit var userOutputService : UserOutputService


    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = BloodpressureeditformBinding.inflate(inflater,container,false)
        initializeService(binding.root)

        return binding.root
    }

    private fun initializeService(view: View) {
        userOutputService = UserOutputServiceImpl(requireContext(),view)
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    private lateinit var viewModel: BloodPressureViewModel

    var itemID: Int = 0 // id is already taken by Fragment class
    private val timestampCal = Calendar.getInstance()

    private var heartRhythmIssue = false


    private fun stringToInt(valueString: String, errorMessage: String): Int {
        var valueInt = -1
        if (valueString.isNotEmpty()) {
            try {
                valueInt = valueString.toInt()
                if (valueInt <= 9) valueInt = -2
            }
            catch(e: Exception) {
                valueInt = -3
            }
        }
        if  (valueInt < 1) userOutputService.showMessageAndWaitForLong(errorMessage)

        return valueInt
    }

    private fun saveItem() {
        val editItem = viewModel.getItem(itemID)
        if (editItem == null) {
            userOutputService.showMessageAndWaitForLong("Unknown error!")
            return
        }

        // Safely convert string values
        var value: Int = stringToInt(binding.etSys.text.toString(), getString(R.string.sysMissing))
        if (value < 0) return
        editItem.value1 = binding.etSys.text.toString()

        value = stringToInt(binding.etDia.text.toString(), getString(R.string.diaMissing))
        if (value < 0) return
        editItem.value2 = binding.etDia.text.toString()

        value = stringToInt(binding.etPulse.text.toString(), getString(R.string.pulseMissing))
        if (value < 0) return
        editItem.value3 = binding.etPulse.text.toString()

        editItem.timestamp = timestampCal.timeInMillis
        editItem.comment = binding.etComment.text.toString()
        editItem.value4 = if (heartRhythmIssue) "1" else "0"

        viewModel.update(editItem)

        userOutputService.showMessageAndWaitForLong(getString(R.string.itemUpdated))
        requireActivity().onBackPressed()
    }

    private fun setHeartRhythm() {
        heartRhythmIssue = !heartRhythmIssue
        if (heartRhythmIssue) binding.btHeartRhythm.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryDarkRed))
        else binding.btHeartRhythm.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryGreen))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(requireActivity()).get(BloodPressureViewModel::class.java)
        viewModel.init(BLOODPRESSURE)

        val preferences = Preferences.getSharedPreferences(requireContext())

        when (preferences.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue))) {
            this.getString(R.string.green) -> binding.btSave.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryGreen))
            this.getString(R.string.red)   -> binding.btSave.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryRed))
            this.getString(R.string.gray)   -> binding.btSave.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryGray))
            else   -> binding.btSave.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryBlue))
        }
        binding.btSave.setColorFilter(Color.WHITE)

        val editItem = viewModel.getItem(itemID)
        if (editItem == null) {
            userOutputService.showMessageAndWaitForLong("Unknown error!")
            return
        }

        this.binding.etSys.setText(editItem.value1)
        this.binding.etDia.setText(editItem.value2)
        this.binding.etPulse.setText(editItem.value3)

        val logHeartRhythm = preferences.getBoolean(SettingsActivity.KEY_PREF_LOG_HEART_RHYTHM, true)

        if (logHeartRhythm) {
            // Invert result because setHeartRhythm inverst again
            heartRhythmIssue = (editItem.value4 != "1")
            setHeartRhythm()
        }
        else binding.btHeartRhythm.visibility = View.GONE

        // Check if we are really editing or if this is a new value
        if (viewModel.isTmpItem(editItem.comment)) {
            this.binding.etComment.setText("")
            this.binding.etSys.setText("") // Make sure fields are empty to avoid the need to delete the default 0
            this.binding.etDia.setText("")
            this.binding.etPulse.setText("")
        }
        else this.binding.etComment.setText(editItem.comment)

        this.binding.etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(editItem.timestamp)
        this.binding.etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(editItem.timestamp)

        // Respond to click events
        binding.btSave.setOnClickListener { saveItem() }
        binding.btHeartRhythm.setOnClickListener { setHeartRhythm() }

        // ---------------------
        // Date/Time picker section
        timestampCal.timeInMillis = editItem.timestamp

        // create an OnDateSetListener
        val dateListener = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            timestampCal.set(Calendar.YEAR, year)
            timestampCal.set(Calendar.MONTH, monthOfYear)
            timestampCal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            binding.etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
            binding.etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
        }

        val timeListener = TimePickerDialog.OnTimeSetListener { _, hour, minute ->
            timestampCal.set(Calendar.HOUR_OF_DAY, hour)
            timestampCal.set(Calendar.MINUTE, minute)

            binding.etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
            binding.etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
        }

//        btDatePicker!!.setOnClickListener(object : View.OnClickListener {
        binding.etDate.setOnClickListener {
            DatePickerDialog(requireContext(),
                    dateListener,
                    timestampCal.get(Calendar.YEAR),
                    timestampCal.get(Calendar.MONTH),
                    timestampCal.get(Calendar.DAY_OF_MONTH)).show()
        }

        binding.etTime.setOnClickListener {
            TimePickerDialog(requireContext(),
                    timeListener,
                    timestampCal.get(Calendar.HOUR_OF_DAY),
                    timestampCal.get(Calendar.MINUTE),
                    android.text.format.DateFormat.is24HourFormat(requireContext())).show()
        }

        // Make sure first field is highlighted and keyboard is open
        this.binding.etSys.requestFocus()
    }

    fun newInstance(i: Int): BloodPressureEditFragment {
        val f = BloodPressureEditFragment()
        f.itemID = i
        return f
    }

    override fun onPause() {
        super.onPause()
        MainActivity. resetReAuthenticationTimer(requireContext())
        viewModel.deleteTmpItem()
    }
}