package com.zell_mbc.medilog.bloodpressure

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.input.input
import com.zell_mbc.medilog.*
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.data.TabFragment
import com.zell_mbc.medilog.databinding.BloodpressureTabBinding
import java.util.*


class BloodPressureFragment : TabFragment(), BloodPressureListAdapter.ItemClickListener {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: BloodpressureTabBinding? = null
    private val binding get() = _binding!!
    private var adapter: BloodPressureListAdapter? = null
    private var standardColors = 0
    private var comment = ""


    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = BloodpressureTabBinding.inflate(inflater, container, false)
        initializeService(binding.root)

        return binding.root
    }

    private fun stringToInt(valueString: String, errorMessage: String): Int {
        var valueInt = -1
        if (valueString.isNotEmpty()) {
            try {
                valueInt = valueString.toInt()
                if (valueInt <= 9) valueInt = -2
            }
            catch (e: Exception) {
                valueInt = -3
            }
        }
        if  (valueInt < 1) userOutputService.showMessageAndWaitForLong(errorMessage)

        return valueInt
    }

    private fun addItem() {
        // If standard entry is active create a data element and launch the editActivity
        if (!quickEntry) {
            val tmpItem = Data(0, Date().time, MainActivity.tmpComment, viewModel.dataType, "", "", "", "")
            val itemID = viewModel.insert(tmpItem)
            if (itemID  != 0L) editItem(itemID.toInt())
            else userOutputService.showMessageAndWaitForLong("No entry with tmp value found!")
            return
        }

        // ###########################
        // Checks
        // ###########################
        // Savely convert string values
        var value: Int = stringToInt(binding.etSys.text.toString(), getString(R.string.sysMissing))
        if (value < 0) return

        value = stringToInt(binding.etDia.text.toString(), getString(R.string.diaMissing))
        if (value < 0) return

        value = stringToInt(binding.etPulse.text.toString(), getString(R.string.pulseMissing))
        if (value < 0) return

        val item = Data(0, Date().time, comment, viewModel.dataType, binding.etSys.text.toString(), binding.etDia.text.toString(), binding.etPulse.text.toString(), "")

        viewModel.insert(item)
        if ((viewModel.filterEnd > 0L) && (viewModel.filterEnd < item.timestamp)) userOutputService.showMessageAndWaitForDuration(getString(R.string.filteredOut), 4000)

        binding.etSys.setText("")
        binding.etDia.setText("")
        binding.etPulse.setText("")
        binding.etSys.bringToFront()

        // Close keyboard after entry is done
        (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken, 0)
    }

    override fun onItemClick(view: View?, position: Int) {
        val item = adapter?.getItemAt(position)
        item ?: return
        itemClicked(item)
    }

    override fun editItem(index: Int) {
        val intent = Intent(requireContext(), BloodPressureEditActivity::class.java)
        launchActivity(intent, index)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setColourStyle(binding.btAdd)

        // Hide quick entry fields if needed
        if (!quickEntry) {
            binding.etSys.visibility = View.GONE
            binding.etDia.visibility = View.GONE
            binding.etPulse.visibility = View.GONE
            binding.btComment.visibility = View.GONE
            //           btHeartRhythm.visibility = View.GONE
        }
        else {
            binding.etSys.visibility = View.VISIBLE
            binding.etDia.visibility = View.VISIBLE
            binding.etPulse.visibility = View.VISIBLE
            binding.btComment.visibility = View.VISIBLE
            // Todo: Think about ways to offer HeartRhythm without cluttering the screen
            //          btHeartRhythm.visibility = View.VISIBLE
        }

        // set up the RecyclerView
        val layoutManager = LinearLayoutManager(requireContext())
        binding.rvBloodPressureList.layoutManager = layoutManager

        adapter = BloodPressureListAdapter(requireContext())
        viewModel = ViewModelProvider(requireActivity()).get(BloodPressureViewModel::class.java)
        viewModel.items.observe(requireActivity(), { data -> data?.let { adapter!!.setItems(it) } })

        adapter!!.setClickListener(this)
        binding.rvBloodPressureList.adapter = adapter
        val dividerItemDecoration = DividerItemDecoration(binding.rvBloodPressureList.context, layoutManager.orientation)
        binding.rvBloodPressureList.addItemDecoration(dividerItemDecoration)

        // Respond to click events
        binding.btAdd.setOnClickListener { addItem() }
        setColourStyle(binding.btInfo, true)
        setColourStyle(binding.btChart, true)
        setColourStyle(binding.btComment, true)

        binding.btInfo.setOnClickListener(View.OnClickListener {
            context ?: return@OnClickListener
            val intent = Intent(requireContext(), BloodPressureInfoActivity::class.java)
            startActivity(intent)
        })

        binding.btChart.setOnClickListener(View.OnClickListener {
            if (context == null) return@OnClickListener
            if (viewModel.getSize(true) < 2) {
                userOutputService.showMessageAndWaitForLong(requireContext().getString(R.string.notEnoughDataForChart))
                return@OnClickListener
            }

            val intent = Intent(requireContext(), BloodPressureChartActivity::class.java)
            startActivity(intent)
        })

        binding.btAdd.setColorFilter(Color.WHITE)
        standardColors =  binding.etDia.currentHintTextColor //  textColors.defaultColor
//        btHeartRhythm.setTextColor(standardColors) // Make sure the button comes up in correct colour

        binding.btChart.setOnClickListener(View.OnClickListener {
            val c = context ?: return@OnClickListener
            if (viewModel.getSize(true) < 2) {
                userOutputService.showMessageAndWaitForLong(c.getString(R.string.notEnoughDataForChart))
                return@OnClickListener
            }
            val intent = Intent(requireContext(), BloodPressureChartActivity::class.java)
            startActivity(intent)
        })

        binding.btComment.setOnClickListener {
            MaterialDialog(requireContext()).show {
                title(R.string.enterComment)
                input(allowEmpty = true, hintRes = R.string.comment) { _, text ->
                    comment = text.toString()
                }
                positiveButton(R.string.submit)
            }
        }

        binding.etSys.doOnTextChanged { text, start, count, after ->
            if (!text.isNullOrEmpty() ) {
                val first = text[0].toString()
                val jump = if (first == "1") 2 else 1 // If first char is 1, the final value will be 3 digits, else two digits
                if (text.length > jump) binding.etDia.requestFocus()
            }
        }

        binding.etDia.doOnTextChanged { text, start, count, after ->
            if (!text.isNullOrEmpty()) {
                val first = text[0].toString()

                val jump = if (first == "1") 2 else 1 // If first char is 1, the final value will be 3 digits, else two digits
                if (text.length > jump) binding.etPulse.requestFocus()
            }
        }

        binding.etPulse.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if ((event.action == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                addItem()
                return@OnKeyListener true
            }
            false
        })
    }
}