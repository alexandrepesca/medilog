package com.zell_mbc.medilog.bloodsugar

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.BLOODSUGAR
import com.zell_mbc.medilog.MainActivity.Companion.BLOODSUGAR_UNIT_DEFAULT
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.databinding.BloodsugareditformBinding
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utillity.Preferences
import java.text.DateFormat
import java.util.*

class BloodSugarEditFragment : Fragment() {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: BloodsugareditformBinding? = null
    private val binding get() = _binding!!
    private lateinit var userOutputService : UserOutputService


    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = BloodsugareditformBinding.inflate(inflater,container,false)
        initializeService(binding.root)

        return binding.root
    }
    private fun initializeService(view: View) {
        userOutputService = UserOutputServiceImpl(requireContext(),view)
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private lateinit var viewModel: BloodSugarViewModel  //by viewModels() //factoryProducer = { SavedStateViewModelFactory(this, ) })
    private val timestampCal = Calendar.getInstance()
    var itemID: Int = 0

    private fun saveItem() {
        val editItem = viewModel.getItem(itemID)
        if (editItem == null) {
            userOutputService.showMessageAndWaitForLong("Unknown error!")
            return
        }

        // Check empty variables
        val value = binding.etBloodSugar.text.toString()
        if (value.isEmpty()) {
            userOutputService.showMessageAndWaitForLong(this.getString(R.string.bloodSugarMissing))
            return
        }

        // Valid bloodSugar?
        var bloodSugarValue = 0
        try {
            bloodSugarValue = value.toInt()
            if (bloodSugarValue <= 0) {
                userOutputService.showMessageAndWaitForLong(this.getString(R.string.invalid) + " " + this.getString(R.string.bloodSugar) + " " + this.getString(R.string.value) + " $bloodSugarValue")
                return
            }
        } catch (e: Exception) {
            userOutputService.showMessageAndWaitForLong("Exception: " + this.getString(R.string.invalid) + " " + this.getString(R.string.bloodSugar) + " " + this.getString(R.string.value) + " $bloodSugarValue")
            return
        }

        editItem.timestamp = timestampCal.timeInMillis
        editItem.value1 = bloodSugarValue.toString()
        editItem.comment = binding.etComment.text.toString()

        viewModel.update(editItem)

        userOutputService.showMessageAndWaitForLong(requireContext().getString(R.string.itemUpdated))
//        Log.d("BloodSugarEditFragment", "Finish: ")
        requireActivity().onBackPressed()
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(requireActivity()).get(BloodSugarViewModel::class.java)
        viewModel.init(BLOODSUGAR)

        val preferences = Preferences.getSharedPreferences(requireContext())
        val colourStyle = preferences.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue))
        val saveButton = binding.btSave

        when (colourStyle) {
            this.getString(R.string.green) -> binding.btSave.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryGreen))
            this.getString(R.string.red)   -> binding.btSave.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryRed))
            this.getString(R.string.gray)   -> binding.btSave.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryGray))
            else   -> binding.btSave.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryBlue))
        }
        saveButton.setColorFilter(Color.WHITE)

        binding.textUnit.text = preferences.getString(SettingsActivity.KEY_PREF_BLOODSUGAR_UNIT, BLOODSUGAR_UNIT_DEFAULT)

        val editItem = viewModel.getItem(itemID)
        if (editItem == null) {
            userOutputService.showMessageAndWaitForLong("Unknown error!")
            return
        }

        binding.etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(editItem.timestamp)
        binding.etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(editItem.timestamp)

        // Check if we are really editing or if this is a new value, if new entry delete tmpComment
        if (viewModel.isTmpItem(editItem.comment))
            binding.etComment.setText("")
        else {
            binding.etBloodSugar.setText(editItem.value1)
            binding.etComment.setText(editItem.comment)
        }

        // Respond to click events
        saveButton.setOnClickListener { saveItem() }

        // ---------------------
        // Date/Time picker section
        timestampCal.timeInMillis = editItem.timestamp

        // create an OnDateSetListener
        val dateListener = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            timestampCal.set(Calendar.YEAR, year)
            timestampCal.set(Calendar.MONTH, monthOfYear)
            timestampCal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            binding.etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
            binding.etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
        }

        val timeListener = TimePickerDialog.OnTimeSetListener { _, hour, minute ->
            timestampCal.set(Calendar.HOUR_OF_DAY, hour)
            timestampCal.set(Calendar.MINUTE, minute)
            binding.etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
            binding.etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
//        Log.d("--------------- Debug", "24hour: $b")
        }

        binding.etDate.setOnClickListener {
            DatePickerDialog(requireContext(),
                    dateListener,
                    timestampCal.get(Calendar.YEAR),
                    timestampCal.get(Calendar.MONTH),
                    timestampCal.get(Calendar.DAY_OF_MONTH)).show()
        }

        binding.etTime.setOnClickListener {
            TimePickerDialog(requireContext(),
                    timeListener,
                    timestampCal.get(Calendar.HOUR_OF_DAY),
                    timestampCal.get(Calendar.MINUTE),
                    android.text.format.DateFormat.is24HourFormat(requireContext())).show()
        }

        // Make sure first field is highlighted and keyboard is open
        binding.etBloodSugar.requestFocus()

    }

    override fun onPause() {
        super.onPause()
        MainActivity. resetReAuthenticationTimer(requireContext())
        viewModel.deleteTmpItem() // Deletes all items with comment == tmpComment
    }

    fun newInstance(i: Int): BloodSugarEditFragment {
        val f = BloodSugarEditFragment()
        // Supply index input as an argument.
        f.itemID = i
//        Log.d("--------------- Debug", "NewInstance Index: " + itemID)
        return f
    }
}
