package com.zell_mbc.medilog.bloodsugar

import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.utillity.Preferences
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.zell_mbc.medilog.*
import com.zell_mbc.medilog.MainActivity.Companion.BLOODSUGAR_UNIT_DEFAULT
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.data.TabFragment
import com.zell_mbc.medilog.databinding.BloodsugarTabBinding
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.weight.WeightEditActivity
import java.text.DateFormat
import java.util.*

class BloodSugarFragment : TabFragment(), BloodSugarListAdapter.ItemClickListener {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: BloodsugarTabBinding? = null
    private val binding get() = _binding!!
    private var adapter: BloodSugarListAdapter? = null


    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = BloodsugarTabBinding.inflate(inflater,container,false)
        initializeService(binding.root)

        return binding.root
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun addItem() {

        if (!quickEntry) {
            val tmpItem = Data(0, Date().time, MainActivity.tmpComment, viewModel.dataType, "","","", "")
            val itemID = viewModel.insert(tmpItem)
            if (itemID  != 0L) editItem(itemID.toInt())
            else userOutputService.showMessageAndWaitForLong("No entry with tmp value found!")
            return
        }

        // Check empty variables

        val bloodSugarValue = binding.etBloodSugar.text.toString()
        val commentValue = binding.etComment.text.toString()
        if (bloodSugarValue.isEmpty()) {
            userOutputService.showMessageAndWaitForLong(getString(R.string.bloodSugarMissing))
            return
        }
        var glycose = 0
        try {
            glycose = bloodSugarValue.toInt()
            if (glycose <= 0) {
                userOutputService.showMessageAndWaitForLong(this.getString(R.string.invalid) + " " + this.getString(R.string.bloodSugar) + " " + this.getString(R.string.value) + " $glycose")
                return
            }
        } catch (e: Exception) {
            userOutputService.showMessageAndWaitForLong("Exception: " + this.getString(R.string.invalid) + " " + this.getString(R.string.bloodSugar) + " " + this.getString(R.string.value) + " $glycose")
            return
        }
        val item = Data(0,  Date().time,commentValue, viewModel.dataType, glycose.toString(), "","","") // Start with empty item

        viewModel.insert(item)

        if ((viewModel.filterEnd > 0L) && (viewModel.filterEnd < item.timestamp)) userOutputService.showMessageAndWaitForDuration(getString(R.string.filteredOut), 4000)

        binding.etBloodSugar.setText("")
        binding.etComment.setText("")

        // Close keyboard after entry is done
        (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken, 0)
    }

    override fun onItemClick(view: View?, position: Int) {
        val item = adapter?.getItemAt(position)
        item ?: return
        itemClicked(item)
    }

    override fun editItem(index: Int) {
        val intent = Intent(requireContext(), BloodSugarEditActivity::class.java)
        launchActivity(intent, index)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setColourStyle(binding.btAdd)
        binding.btAdd.setColorFilter(Color.WHITE)

        if (!quickEntry) { // Hide quick entry fields
            binding.etBloodSugar.visibility = View.GONE
            binding.etComment.visibility = View.GONE
            binding.textUnit.visibility = View.GONE
        }
        else {
            binding.etBloodSugar.visibility = View.VISIBLE
            binding.etComment.visibility = View.VISIBLE
            binding.textUnit.visibility = View.VISIBLE
            binding.textUnit.text = preferences.getString(SettingsActivity.KEY_PREF_BLOODSUGAR_UNIT, BLOODSUGAR_UNIT_DEFAULT)
        }

        val layoutManager = LinearLayoutManager(requireContext())
        binding.rvBloodSugarList.layoutManager = layoutManager

        adapter = BloodSugarListAdapter(requireContext())
        viewModel = ViewModelProvider(requireActivity()).get(BloodSugarViewModel::class.java)  // This should return the MainActivity ViewModel
        viewModel.items.observe(requireActivity(), { data -> data?.let { adapter!!.setItems(it) } })

        adapter!!.setClickListener(this)
        binding.rvBloodSugarList.adapter = adapter
        val dividerItemDecoration = DividerItemDecoration(binding.rvBloodSugarList.context, layoutManager.orientation)
        binding.rvBloodSugarList.addItemDecoration(dividerItemDecoration)

        // Respond to click events
        binding.btAdd.setOnClickListener { addItem() }
        setColourStyle(binding.btInfo, true)
        setColourStyle(binding.btChart, true)

        binding.btInfo.setOnClickListener {
            val intent = Intent(requireContext(), BloodSugarInfoActivity::class.java)
            startActivity(intent)
        }

        binding.btChart.setOnClickListener(View.OnClickListener {
            val c = context
            if (c == null) {
                Log.d("Debug OpenChart: ", "Empty Context")
                return@OnClickListener
            }
            if (viewModel.getSize(true) < 2) {
                userOutputService.showMessageAndWaitForLong(c.getString(R.string.notEnoughDataForChart))
                return@OnClickListener
            }

            val intent = Intent(requireContext(), BloodSugarChartActivity::class.java)
            startActivity(intent)
        })

        binding.etBloodSugar.doOnTextChanged { text, start, count, after ->
            if (!text.isNullOrEmpty()) {
                val first = text[0].toString()

                val jump = if (first == "1") 2 else 1 // If first char is 1, the final value will be 3 digits, else two digits
                if (text.length > jump) binding.etComment.requestFocus()
            }
        }

        binding.etComment.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if ((event.action == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                addItem()
                return@OnKeyListener true
            }
            false
        })

    }
}
