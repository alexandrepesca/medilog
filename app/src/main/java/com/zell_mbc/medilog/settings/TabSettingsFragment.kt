package com.zell_mbc.medilog.settings

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.preference.MultiSelectListPreference
import com.takisoft.preferencex.PreferenceFragmentCompat
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.SettingsViewModel
import java.util.ArrayList

class TabSettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferencesFix(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.tabspreferences, rootKey)

        // Populate tab selection array
        findPreference<MultiSelectListPreference>(SettingsActivity.KEY_PREF_ACTIVE_TABS_SET)?.apply {
            // Check if any custom tabs are present
  //          var entryDisplay    = listOf(getString(R.string.diary), getString(R.string.bloodPressure), getString(R.string.bloodSugar), getString(R.string.water), getString(R.string.weight) ).toTypedArray()

            val newActiveTabs = ArrayList<String>()
            // Default, named tabs
            newActiveTabs.add(getString(R.string.bloodPressure))
            newActiveTabs.add(getString(R.string.weight))
            newActiveTabs.add(getString(R.string.diary))
            newActiveTabs.add(getString(R.string.water))
            newActiveTabs.add(getString(R.string.bloodSugar))

            // Custom tabs
            val settings = ViewModelProvider(requireActivity()).get(SettingsViewModel::class.java)
            settings.init()
//            if (settings.countDataTypes() > 0) {
                // Add custom Types
//                for (t in settings.getDataTypes()) {
//                    newActiveTabs.add(settings.getString(t, "TAB_LABEL"))
//                }
//            }
            val entryDisplay    = newActiveTabs.toTypedArray()
            val entryValuesApps = newActiveTabs.toTypedArray()

            entries = entryDisplay
            this.entryValues = entryValuesApps
        }
    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
    }

}
