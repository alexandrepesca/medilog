package com.zell_mbc.medilog.settings

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.text.InputFilter
import android.text.Spanned
import android.view.View
import androidx.preference.Preference
import com.takisoft.preferencex.EditTextPreference
import com.takisoft.preferencex.PreferenceFragmentCompat
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.bloodpressure.BloodPressureHelper
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl

class BloodPressureSettingsFragment : PreferenceFragmentCompat() {
    lateinit var userOutputService : UserOutputService

    private fun initializeService(context: Context, view: View) {
        userOutputService = UserOutputServiceImpl(context,view)
    }

    override fun onCreatePreferencesFix(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.bloodpressurepreferences, rootKey)

        class GradeInputFilter: InputFilter {
            private val filterString = "0123456789,"

            override fun filter(source: CharSequence, start: Int, end: Int, dest: Spanned, dstart: Int, dend: Int): CharSequence? {
                initializeService(requireContext(),requireView())

                try {
                    if (filterString.indexOf(source.toString()) >= 0) return null
                    else
                    //if (input.isNotEmpty())
                        userOutputService.showMessageAndWaitForLong(requireContext().getString(R.string.allowedInput) + " $filterString")
                } catch (nfe: NumberFormatException) { return "" }  // Define abstract function
                return ""
            }
        }

        val editPrefs = ArrayList<EditTextPreference?>()
        editPrefs.add(preferenceManager.findPreference("grade1"))
        editPrefs.add(preferenceManager.findPreference("grade2"))
        editPrefs.add(preferenceManager.findPreference("grade3"))

        for (editTextPreference in editPrefs)
            if (editTextPreference != null) {
                editTextPreference.setOnBindEditTextListener { editText ->
                    val filterArray = arrayOfNulls<InputFilter>(2)
                    filterArray[0] = InputFilter.LengthFilter(7)
                    filterArray[1] = GradeInputFilter()
                    editText.filters = filterArray
                }

                editTextPreference.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue ->
                    var rtnval = true
                    if (newValue.toString().isEmpty()) {
                        val builder: AlertDialog.Builder = AlertDialog.Builder(activity)
                        builder.setTitle(getString(R.string.invalidInput))
                        builder.setMessage(getString(R.string.emptyGrade))
                        builder.setPositiveButton(android.R.string.ok, null)
                        builder.show()
                        rtnval = false
                    }
                    rtnval
                }
            }
    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
        BloodPressureHelper(requireContext())
    }
}