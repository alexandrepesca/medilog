package com.zell_mbc.medilog.settings

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.utillity.Preferences


class SettingsActivity : AppCompatActivity(), PreferenceFragmentCompat.OnPreferenceStartFragmentCallback {

        override fun onPreferenceStartFragment(caller: PreferenceFragmentCompat, pref: Preference): Boolean {
            // Instantiate the new Fragment
            val args = pref.extras
            val fragment = supportFragmentManager.fragmentFactory.instantiate(
                    classLoader,
                    pref.fragment)
            fragment.arguments = args
            fragment.setTargetFragment(caller, 0)
            // Replace the existing Fragment with the new Fragment
            supportFragmentManager.beginTransaction()
                    .replace(android.R.id.content, fragment)
                    .addToBackStack(null)
                    .commit()
            return true
        }

        override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainActivity.setTheme(this)

        val preferences = Preferences.getSharedPreferences(this)

        // Copy activeTabs to HashSet
        val newStrSet: MutableSet<String> = HashSet()
        for (tab in MainActivity.activeTabs) { newStrSet.add(tab) }

        val editor = preferences.edit()
        editor.putStringSet(KEY_PREF_ACTIVE_TABS_SET, newStrSet)
        editor.apply()

        supportFragmentManager.beginTransaction().replace(android.R.id.content, SettingsFragment()).commit()
    }

    companion object {
        const val KEY_PREF_DELIMITER = "delimiter"

        const val KEY_PREF_BLOODSUGAR_UNIT         = "etBloodSugarUnit"
        const val KEY_PREF_BLOODSUGAR_LOWER_THRESHOLD    = "etBloodSugarLowerThreshold"
        const val KEY_PREF_BLOODSUGAR_UPPER_THRESHOLD    = "etBloodSugarUpperThreshold"
        const val KEY_PREF_showBloodSugarThresholds    = "cbShowBloodSugarThresholds"
        const val KEY_PREF_showBloodSugarGrid     = "cbShowBloodSugarGrid"
        const val KEY_PREF_showBloodSugarLegend   = "cbShowBloodSugarLegend"

        const val KEY_PREF_WEIGHT_UNIT         = "etWeightUnit"
        const val KEY_PREF_BODY_HEIGHT        = "etHeight"
        const val KEY_PREF_WEIGHT_THRESHOLD    = "etWeightThreshold"
        const val KEY_PREF_SHOW_WEIGHT_THRESHOLD = "cbShowWeightThreshold"
        const val KEY_PREF_SHOW_WEIGHT_GRID     = "cbShowWeightGrid"
        const val KEY_PREF_SHOW_WEIGHT_LEGEND   = "cbShowWeightLegend"
        const val KEY_PREF_WEIGHT_LINEAR_TRENDLINE = "cbWeightLinearTrendline"
        const val KEY_PREF_WEIGHT_MOVING_AVERAGE_TRENDLINE = "cbWeightMovingAverageTrendline"
        const val KEY_PREF_WEIGHT_MOVING_AVERAGE_SIZE = "etWeightMovingAverageSize"
        const val KEY_PREF_WEIGHT_DAY_STEPPING = "cbWeightDayStepping"
        const val KEY_PREF_WEIGHT_BAR_CHART = "cbWeightBarChart"

        const val KEY_PREF_WATER_THRESHOLD     = "evWaterThreshold"
        const val KEY_PREF_WATER_UNIT          = "evWaterUnit"
        const val KEY_PREF_SUMMARYPDF         = "swShowSummaryinPDF"
        const val KEY_PREF_WATER_BAR_CHART      = "cbWaterBarChart"
        const val KEY_PREF_SHOW_WATER_THRESHOLD = "cbShowWaterThreshold"
        const val KEY_PREF_SHOW_WATER_GRID      = "cbShowWaterGrid"
        const val KEY_PREF_showWaterLegend    = "cbShowWaterLegend"

        const val KEY_PREF_BLOODPRESSURE_UNIT         = "etBloodPressureUnit"
        const val KEY_PREF_LOG_HEART_RHYTHM           = "cbLogHeartRhythm"
        const val KEY_PREF_showBloodPressureThreshold = "cbShowBloodPressureThreshold"
        const val KEY_PREF_showBloodPressureGrid      = "cbShowBloodPressureGrid"
        const val KEY_PREF_showBloodPressureLegend    = "cbShowBloodPressureLegend"
        const val KEY_PREF_bpBarChart                 = "cbBloodPressureBarChart"
        const val KEY_PREF_bpDayStepping = "cbBloodPressureDayStepping"
        const val KEY_PREF_SHOWPULSE = "cbShowPulse"
        const val KEY_PREF_hyperGrade3 = "grade3"
        const val KEY_PREF_hyperGrade2 = "grade2"
        const val KEY_PREF_hyperGrade1 = "grade1"
        const val KEY_PREF_BLOODPRESSURE_LINEAR_TRENDLINE = "cbBloodPressureLinearTrendline"
        const val KEY_PREF_BLOODPRESSURE_MOVING_AVERAGE_TRENDLINE = "cbBloodPressureMovingAverageTrendline"
        const val KEY_PREF_BLOODPRESSURE_MOVING_AVERAGE_SIZE = "etBloodPressureMovingAverageSize"

        const val KEY_PREF_BACKUP_WARNING  = "etBackupWarning"
        const val KEY_PREF_AUTO_BACKUP     = "etAutoBackup"
        const val KEY_PREF_LAST_BACKUP     = "tvLastBackup"
        const val KEY_PREF_BACKUP_URI      = "tvBackupUri"

        const val KEY_PREF_ACTIVE_TABS_SET      = "liActiveTabs"
        const val KEY_PREF_SHOWTABICON          = "showTabIcon"
        const val KEY_PREF_SHOWTABTEXT          = "showTabText"
        const val KEY_PREF_SCROLLABLETABS       = "swScrollableTabs"
        const val KEY_PREF_TABTRANSITIONS       = "liTabTransitions"

        const val KEY_PREF_COLOUR = "cbSetColour"
        const val KEY_PREF_TEXT_SIZE = "listTextSize"
        const val KEY_PREF_USER = "userName"
        const val KEY_PREF_COLOUR_STYLE = "colourStyle"
        const val KEY_PREF_PASSWORD = "zipPassword"
        const val KEY_PREF_BIOMETRIC = "enableBiometric"
        const val KEY_PREF_QUICKENTRY = "quickEntry"
    }
}