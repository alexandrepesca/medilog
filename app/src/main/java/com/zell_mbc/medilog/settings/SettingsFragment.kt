package com.zell_mbc.medilog.settings

import android.os.Bundle
import com.takisoft.preferencex.PreferenceFragmentCompat
import com.zell_mbc.medilog.R

class SettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferencesFix(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)
    }
}