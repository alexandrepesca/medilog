package com.zell_mbc.medilog.settings

import android.os.Bundle
import com.takisoft.preferencex.PreferenceFragmentCompat
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R

class DiarySettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferencesFix(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.diarypreferences, rootKey)
    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
    }

}