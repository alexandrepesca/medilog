package com.zell_mbc.medilog.settings

import android.content.Context
import android.os.Bundle
import androidx.preference.Preference
import com.takisoft.preferencex.EditTextPreference
import com.takisoft.preferencex.PreferenceFragmentCompat
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl

class WeightSettingsFragment : PreferenceFragmentCompat() {

    lateinit var userOutputService : UserOutputService
    override fun onCreatePreferencesFix(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.weightpreferences, rootKey)

        initializeService(this.requireContext())

        val weightThreshold = preferenceManager.findPreference<EditTextPreference>(SettingsActivity.KEY_PREF_WEIGHT_THRESHOLD)
        if (weightThreshold != null)
        {
            weightThreshold.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue ->
                val `val` = newValue.toString().toInt()
                if (`val` < 20 || `val` > 200) {
                    // invalid you can show invalid message
                    userOutputService.showAndHideMessageForLong("Choose a value between 20 and 200")
                    false
                } else {
//                    preference.summary = "" + `val`
                    true
                }
            }
        }
    }

    private fun initializeService(context: Context) {
        userOutputService = UserOutputServiceImpl(context,null)
    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
    }
}