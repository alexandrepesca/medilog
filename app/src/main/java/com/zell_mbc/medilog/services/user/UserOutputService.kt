package com.zell_mbc.medilog.services.user

import android.view.View

interface UserOutputService {
    fun showAndHideMessageForLong(messageAsResourceId: Int)
    fun showAndHideMessageForLong(messageAsCharSequence: CharSequence)

    fun showAndHideMessageForShort(messageAsResourceId: Int)
    fun showAndHideMessageForShort(messageAsCharSequence: CharSequence)

    fun showMessageAndWaitForLong(messageAsResourceId: Int)

    fun showMessageAndWaitForLong(messageAsString: CharSequence)
    fun showMessageAndWaitForDuration(messageAsCharSequence: CharSequence, durationAsMilliseconds: Int)

    fun showMessageAndWaitForDurationAndAction(messageAsCharSequence: CharSequence, durationAsMilliseconds: Int, actionMessageAsCharSequence: CharSequence, actionListener: View.OnClickListener, actionTextColor: Int)
}