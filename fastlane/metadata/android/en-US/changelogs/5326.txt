## v2.0, build 5326
New 

- Support for older devices, Lollipop (Android 5) and upwards
- Added support for Blood Sugar logging (experimental)
- Added Pan&Zoom for charts
- New logo
- New Info section for each logged value
- Allow to reorder tabs
- Moved BloodPressure Comment field to separate input Dialog to save space
- Better looking charts
- Significant internal code changes to support future addition of additional values to be logged

Fixed

- None

Known issues

- None
