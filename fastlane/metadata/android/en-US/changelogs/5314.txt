## v1.8.1.2, build 5314
New 
- Nothing

Fixed
- Fixed a bug where "send CSV" would lead to a "PDF not found" error message


Known issues
- Setting/Changing the filter is not reflected in the data tabs right away. Needs a manual screen refresh via launching About or Settings screen or, of course restarting the app
