## v2.1.2, build 5335
New 

Fixed/Changed

- Fixed bug which led to customTab code being executed. Present an error message instead

Known issues

- Sometimes the first Edit field (E.g. Systolic or Weight) in a tab loses focus as soon as the first digit is entered

