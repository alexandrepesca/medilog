## v1.9.0.0, build 5316
New 
- Added Heart Rhythm logging
- Added Water Intake logging
- Basic Water Intake reporting

Fixed
- Fixed bug where Weight edit wouldn't load the comment
- Setting/Changing the filter is now reflected in the data tabs right away.

Known issues
- Water Intake settings not (yet) exposed to user
- Water Intake report not (yet) showing summary
- Water Intake main screen not (yet) showing summary of daily intake
