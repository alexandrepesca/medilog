## v1.7.6, build 5309
- Added | as acceptable CSV field delimiter
- Added check for empty CSV field delimiter in settings dialog
- Added pin authentication as fallback for biometric logon (disabled for now until https://issuetracker.google.com/issues/142740104 is fixed)
- More delimiter checking and better feedback in case of errors during import

Fixed
- Fixed some documentation inconsistencies in regards to version numbers, build number, etc.
- Disable biometric logon instead of hiding if biometric is not available/set up

