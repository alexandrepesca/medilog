## v2.0.6, build 5332
New 

-  Added time to lastBackup in About and BackupSettings

Fixed

- Fix bug which prevented Systolic value to show in PDF report when HighlightValues was set to off
- Changed PDF report so it respects the log-heart-rhythm-issues setting.

Known issues

- None
