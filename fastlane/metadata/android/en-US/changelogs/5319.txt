## v1.9.0.3, build 5319
New 

- Translations (thank you https://mastodon.technology/@mondstern)
    + Spanish
    + Danish
    + Dutch
    + Italian
    + German
    + French


Fixed

- Fixed error where About and chart screens would block re-authentication after inactive time expired 
- Switched to older Gradle version to make things compile with F-Droid 
- Added state highlighting to Diary PDF report. "Not good" = Italics, Bad = bold italics
- Fixed a crash when trying to filter an empty table
- Fixed an issue where the filter icon is not reset after a data import

Known issues

- Water Intake report not (yet) showing summary
